% FUN_cargar_VOP
% recibe el nombre de un archivo
% devuelve una matriz de 10000*2 con las señales de presion (2 columnas)
function [signals]=FUN_cargar_VOP(filename)
    fid=fopen(filename);
    if fid==-1
        disp('Error al abrir el archivo.');
        signals= [-1 -1]
        return;
    end
    data=fread(fid,'uint8')';
    fclose(fid);
    data=data';
    N=10000;
    AUX=[data(209:8:80201) data(210:8:80202) data(211:8:80203) data(212:8:80204)];
    for i= 1:N
        CH1(i)=typecast(uint8(AUX(i,:)),'single');
    end
    AUX=[data(213:8:80205) data(214:8:80206) data(215:8:80207) data(216:8:80208)];
    for i= 1:N
        CH2(i)=typecast(uint8(AUX(i,:)),'single');
    end
    signals=[CH1' CH2'];
end