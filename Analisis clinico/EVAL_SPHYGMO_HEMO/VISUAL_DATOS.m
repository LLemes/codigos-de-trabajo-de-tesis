%==========================================================================
%EVALUACION DATOS PROTOCOLO PAC (SPHYGMOCOR) - VOP - FMD (HEMODYN)
%==========================================================================
%
%Descripcion de la informacion adquirida para cada individuo
%
%1) DISPOSITIVO SPHYGMOCOR XCEL============================================
%
%Se evaluo Presion Arterial Central (PAC) y Velocidad de la onda del pulso 
%(VOP)
%
% XXX_XXX051_C_AV_PULSE    -> 1 latido CALIBRADO de presion CENTRAL
% XXX_XXX051_P_RAW_SIGNALS -> Varios latidos SIN CALIBRAR de presion BRAQUIAL 
% XXX_XXX016_pwv           -> Latidos NO CALIBRADOS de Carotida y Femoral
%                             para determinar VOP 
%
% Frecuencia de muestreo: 128Hz
%
%2) DISPOSITIVO HEMODYN====================================================
%
% Se proporciona un archivo VOP cf.VOP que debe abrirse con el script:
%
%                 P=FUN_cargar_VOP('VOP cf.vop']);
%
% El script devuelve una matriz de Nx2 donde:
% Primera columna: PRESION CAROTIDEA
% Segunda columna: PRESION FEMORAL
%
% Frecuencia de muestreo: 1000Hz
%
%==========================================================================
clear;clc;close all; 

%% 1) Lectura y visualizacion de datos SPHYGMOCOR XCEL-----------------------
figure;
fs=128;

%Presion BRAQUIAL Pulsos sin calibrar
PAC_SC=load([pwd '/Datos ROULLIER/xcel/Roullier_Walker_28472136_20151120_111051_P_RAW_SIGNALS.txt']);
t_PAC_SC=0:1/fs:(length(PAC_SC)-1)/fs;
subplot(1,2,1),plot(t_PAC_SC,PAC_SC);axis tight;
xlabel('Tiempo [s]');legend('Presion BRAQUIAL medida [s/c]');title('Señal SPHYGMOCOR');

%Presion CENTRAL 1 Pulso Calibrado
PAC=load([pwd '/Datos ROULLIER/xcel/Roullier_Walker_28472136_20151120_111051_C_AV_PULSE.txt']);
t_PAC=0:1/fs:(length(PAC)-1)/fs;
subplot(1,2,2),plot(t_PAC,PAC);axis tight;
xlabel('Tiempo [s]');legend('Presion CENTRAL Estimada [mmHg]');title('Señal SPHYGMOCOR');

%Velocidad de la onda del pulso
figure;
P_CF=load([pwd '/Datos ROULLIER/xcel/Roullier_Walker_28472136_20151120_112016_pwv.txt']);

%Presion CAROTIDA
PC=P_CF(:,1);

%Presion FEMORAL
PF=P_CF(:,2);
t=0:1/fs:(length(PC)-1)/fs;
plot(t,PC,t,PF);axis tight;
xlabel('Tiempo [s]');legend('Presion CAROTIDEA [s/c]','Presion FEMORAL [s/c]');title('Señales SPHYGMOCOR');

%% 2) Lectura y visualizacion de datos HEMODYN-------------------------------
figure;
fs=1000;
P=FUN_cargar_VOP([pwd '/Datos ROULLIER/hemodyn/VOP cf.vop']);

%Presion CAROTIDEA
PC=P(:,1);

%Presion FEMORAL
PF=P(:,2);
t=0:1/fs:(length(PC)-1)/fs;
plot(t,PC,t,PF);axis tight;
xlabel('Tiempo [s]');legend('Presion CAROTIDEA [s/c]','Presion FEMORAL [s/c]');title('Señales HEMODYN');

