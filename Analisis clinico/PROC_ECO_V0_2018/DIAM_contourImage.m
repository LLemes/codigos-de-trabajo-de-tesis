% ======================================================================
% @brief Obtiene una imagen de contornos a partir de una imagen y un valor
% de alfa. Utiliza la funcion imcontour para obtener las isolineas
% representativas de la imagen.
%
% @param imagenIn: Matriz con la imagen a recortar
% @param alfa: parametro de ajuste
%
% @retval cont: matriz de contornos (x,y)
% @retval s: struct de lineas de contorno para todas las isolineas mas
% largas que el ancho de la imagen
% @retval handle: handle a la figura de contorno
% ======================================================================

function [cont, s, handle] = DIAM_contourImage( imagenIn, alfa )

if nargin < 2
    alfa = .17;
end
if max(size(alfa))>1
    [cont, handle] = imcontour(imagenIn,alfa,'r');
else
    [cont, handle] = imcontour(imagenIn,[floor(mean(mean(imagenIn))*alfa) floor(mean(mean(imagenIn))*alfa)],'r');
end

s=DIAM_getContourLines(cont);

end

