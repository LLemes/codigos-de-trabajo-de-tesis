% ======================================================================
% @brief Recibe los puntos definidos en imt y la estructura de isolineas
% que representan la pared arterial para llevar a cabo la interseccion
% entre las lineas verticales definidas a partir del parametro x de cada
% punto definido y la pared arterial.
%
% @param pointTable : tabla de puntos definida dentro de la GUI imt
%                 s : estructura de isolineas representativas de la arteria
%                 size_frame: tamaño de la imagen para obtener los limites
% 
% @retval (x, y) : puntos de interseccion entre las isolinas y lineas
% verticales para su posterior medicion
% ======================================================================

function [ x, y ] = DIAM_findIntersections(pointTable, s, size_frame)

kk = 1; pp = 1; jj = 1;
xlim = size(pointTable);
xlim = xlim(1);
ylim = max(size_frame);
vertLine_y = [1 : ylim];
vertLine_x = zeros(xlim,ylim);
x = zeros(xlim,2);
y = zeros(xlim,2);

for ii =  1 : xlim
    vertLine_x(ii,1 : ylim) = pointTable(ii,1);
end

for pp = 1 : xlim
	for jj = 1 : max(size(s))
        [aux_x, aux_y] = polyxpoly(s(jj).x, s(jj).y, vertLine_x(pp,:), vertLine_y );
        if ~isempty(aux_y)
            if max(aux_y) < max(size_frame)/2
                x(pp,kk) = max(aux_x);
                y(pp,kk) = max(aux_y);
            else
                x(pp,kk) = min(aux_x);
                y(pp,kk) = min(aux_y);          
            end
            kk = kk + 1;
        end
    end
    kk = 1;
end

end

