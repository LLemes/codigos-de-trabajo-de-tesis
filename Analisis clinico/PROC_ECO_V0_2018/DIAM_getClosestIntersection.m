% ======================================================================
% @brief Recibe las intersecciones obtenidas con findIntersections y
% permite obtener las primeras intersecciones desplazandose a partir desde
% el centro de la imagen hacia los extremos superior e inferior.
%
% @param (x, y): puntos de interseccion entre las isolinas y lineas
%                           verticales para su posterior medicion
%        (~, mouse_y) : punto presionado con el mouse.
% 
% @retval (x, y) : puntos de interseccion entre lineas verticales y las
% paredes arteriales para su posterior medicion
% ======================================================================

function [ out_x, out_y ] = DIAM_getClosestIntersection( x, y, mouse_y )
y = sort(y,'ascend');
szX = size(x);
szY = size(y);
minValue = 1;
maxValue = max(y);

for kk = 1 : szY(1)
    for jj = 1 : szY(2)
        if maxValue > y(kk,jj) && y(kk,jj) > mouse_y
            maxValue = y(kk,jj);
        end
        if minValue < y(kk,jj) && y(kk,jj) < mouse_y
            minValue = y(kk,jj);
        end
    end
    for jj = 1 : 2
        if jj == 1
            if minValue > 1
                out_y(kk, jj) = minValue;
            end
        else
            out_y(kk, jj) = maxValue;
        end
    end
    minValue = 1;
    maxValue = mouse_y;
end

for kk = 1 : szX(1)
    for jj = 1 : 2
        if x(kk,jj) > 0
            out_x(kk,jj) = x(kk,jj);
        end
    end
end

end

