% ======================================================================
% @brief Obtiene las isolineas a partir de una matriz de contornos. 
% La matriz debe obtenerse mediante la funcion contour.
%
% @param c: matriz de contorno.
% 
% @retval array s(): 
% v: valor de la isolinea
% x: coordenadas x de la isolinea
% y: coordenadas y de la isolinea
%
% @note
% ======================================================================

function s = DIAM_getContourLines(c)

    sz = size(c,2);                 % Obtengo el tamaño de la matriz de contornos C
    ii = 1; jj = 1;
    
    while ii < sz
        n = c(2,ii);                 % Obtenemos la cantidad de puntos de la isolinea
%    s(jj).v = c(1,ii);           % Valor de la isolinea
        s(jj).x = c(1,ii+1:ii+n);    % Coordenadas X
        s(jj).y = c(2,ii+1:ii+n);    % Coordenadas Y
        jj = jj + 1;                 % Incrementa la cantidad de isolineas
        ii = ii + n + 1;             % Saltea hasta la proxima isolinea
    end

end