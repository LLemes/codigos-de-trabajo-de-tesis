% ======================================================================
% @brief Recibe las intersecciones obtenidas con findIntersections y
% permite obtener las primeras intersecciones desplazandose a partir desde
% el centro de la imagen hacia los extremos superior e inferior.
%
% @param (x, y): puntos de interseccion entre las isolinas y lineas
%                           verticales para su posterior medicion
%                 size_frame : tamaño de la imagen con la que se obtienen los
%                               limites
% 
% @retval (x, y) : puntos de interseccion entre lineas verticales y las
% paredes arteriales para su posterior medicion
% ======================================================================

function [ out_x, out_y ] = DIAM_getFirstIntersections( x, y, size_frame )

szX = size(x);
szY = size(y);
minValue = 1;
maxValue = size_frame(1);

for kk = 1 : szY(1)
    for jj = 1 : szY(2)
        if maxValue > y(kk,jj) && y(kk,jj) > size_frame(1)/2
            maxValue = y(kk,jj);
        end
        if minValue < y(kk,jj) && y(kk,jj) < size_frame(1)/2
            minValue = y(kk,jj);
        end
    end
    for jj = 1 : 2
        if jj == 1
            if minValue > 1
                out_y(kk, jj) = minValue;
            end
        else
            if maxValue < size_frame(1)
                out_y(kk, jj) = maxValue;
            end
        end
    end
    minValue = 1;
    maxValue = size_frame(1);
end

for kk = 1 : szX(1)
    for jj = 1 : 2
        if x(kk,jj) > 0
            out_x(kk,jj) = x(kk,jj);
        end
    end
end

end

