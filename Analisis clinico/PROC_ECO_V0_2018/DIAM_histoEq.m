% ======================================================================
% @brief Recibe una imagen y la devuelve con su histograma ecualizado. Para
% esto ver como funciona ir a : http://angeljohnsy.blogspot.com/2011/04/matlab-code-histogram-equalization.html
%
% @param imagenIn Matriz con la imagen a ecualizar
% 
% @retval imagenOut Matriz con la imagen ecualizada
% ======================================================================


function [ imagenOut ] = DIAM_histoEq( imagenIn)

% figure,imshow(imagenIn);    %% Para debuging

%Specify the bin range[0 255]
bin=255;

%Find the histogram of the image.
Val=reshape(imagenIn,[],1);
Val=double(Val);
I=hist(Val,0:bin);

%Divide the result by number of pixels
Output=I/numel(imagenIn);

%Calculate the Cumlative sum
CSum=cumsum(Output);

%Perform the transformation S=T(R) where S and R in the range [ 0 1]
HIm=CSum(imagenIn+1);


%Convert the image into uint8
imagenOut=uint8(HIm*bin);
% figure,imshow(imagenOut);   %% Para debuging

end

