% ======================================================================
% @brief Remueve los loops cerrados de isolineas obtenidas mediante la 
% funcion getContourLines.
%
%
% @param s: estructura de isolineas obtenida con getContourLines.
%
%
% @retval ret: estructura de isolineas de entrada sin los loops cerrados.
% ======================================================================

function [ s ] = DIAM_removeLoops( s )

sz = max(size(s));
ii = 1; jj = 1;
aux_s = struct('x',[],'y',[]);

for ii = 1 : sz
    szY = max(size(s(ii).y));
    if s(ii).y(1) ~= s(ii).y(szY)
        aux_s(jj).x = s(ii).x;
        aux_s(jj).y = s(ii).y;
        jj = jj + 1;
    end
end

s = aux_s;

end

