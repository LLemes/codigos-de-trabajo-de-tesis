function [Pfd,C, R, Ea] = calculate_pfd(Vfs, Vfd, Pfs, PAC, fs)
    %C = (Vfd-Vfs)/(Pfs-PBD);
    C = (Vfd-Vfs)/(Pfs-PAC(1));
    R_T = mean(PAC)/(Vfd-Vfs);
    R = R_T*(length(PAC)/fs);
%Segers P., Stergiopulos N., Westerhof N. (2001) Relation of effective 
%arterial elastance to arterial systtem properties. Am J Phisiol Heart Circ
%Physiol 282. DOI: 10.1152/ajpheart.00764.2001
    Ea = -0.13 + (1.02*R_T) + (0.31/C); 


    Pfd = Pfs - (Ea*(Vfd-Vfs));
end