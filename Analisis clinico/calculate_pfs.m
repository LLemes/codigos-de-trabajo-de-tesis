function [Pfs, ind_Pfs] = calculate_pfs(Pac)

    dPac = gradient(Pac);
    d2Pac = gradient(dPac);
%     d3Pac = gradient(d2Pac)*fs;
%     d4Pac = gradient(d3Pac)*fs;

    [~,indM_Pac]=findpeaks(Pac,'MinPeakHeight',0.975*max(Pac));

    [~,locm] = findpeaks(-dPac);

    [~,locM2] = findpeaks(d2Pac);

    ind_locm = locm > max(indM_Pac);
    locm = locm(ind_locm);
    
    ind_locM2 = locM2 > locm(1);
    locM2 = locM2(ind_locM2);
    
    ind_Pfs = locM2(1);
    Pfs = Pac(ind_Pfs);

%     figure()
%     t=0:1/fs:(length(Pac)-1)/fs;
%     plot(t,Pac);
%     hold on
%     plot(t(locM2),Pac(locM2),'or');
%     plot(t(locm),Pac(locm),'ok');
%     hold off
end