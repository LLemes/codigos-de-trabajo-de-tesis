function [P, V, PAC, parametros] = cardiac_parameters(path,data, barra)

% Se cargan los datos de presión
    [PAC]=load_C_AV_PULSE(path,data,barra);
    %t_PAC=0:1/fs:(len(1)-1)/fs;

% Cálculo de presión de fin de sístole
    [Pfs,loc_Pfs] = calculate_pfs(PAC);

% Cálculo de presión de fin de diástole
    [Pfd, C, R, Ea] = calculate_pfd(data.VFS, data.VFD, Pfs, PAC, data.fs);

% Area de superficie corporal
    ASC = sqrt((data.Peso*data.Altura)/3600);
    
% Elastancia Máxima
    Emax = Pfs/data.VFS;
    
% V0
    V0 = data.VFS-(Pfs/Ea);
    
% Volumen eyectado sistolico
    VES = (data.VFD-data.VFS);
    
% Cálculo de Cardiac output
    Co = VES*(60/1000)/(length(PAC)/data.fs); %GC=(ml/latido)*(latidos/min)*(l/ml)

% Presión y volumen sistólico
%     ind = logical((cumsum(PAC>=Pfs)<1) + (PAC>=Pfs));
%     sp = PAC(ind,1);
    sp = PAC(1:loc_Pfs,1);
    
    vol_s = linspace(data.VFD,data.VFS,length(sp));

% Presión y volumen diastólico
    sd = ones((length(PAC)-length(sp)),1)*Pfd;
    vol_d = linspace(data.VFS,data.VFD,length(sd));

% Presión y volumen del bucle
    P = [sp; sd; sp(1)];
    V = [vol_s vol_d, vol_s(1)];
    
% Trabajo
    aux_s = trapz(vol_s, sp);
    aux_d = trapz(vol_d, sd);
    W = abs(aux_s) - abs(aux_d);
    
% Jopo del bucle
    aux_s = trapz(vol_s, sp);
    
    aux_p = linspace(Pfs,PAC(1),length(sd));
    aux_d = trapz(vol_d,aux_p);
    Wdis = abs(aux_s) - abs(aux_d);
    
    
% Guardado de parámetros
    parametros = [Pfs, loc_Pfs, Pfd, R, C, Ea, Emax, V0, Co, W, Wdis, ASC];
end