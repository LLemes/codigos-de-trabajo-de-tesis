function [Age, Psys, Pdias, Vfs, Vfd]= extract_health_data(path, id)
    CurrentPath = pwd;
    cd(path);
    file = readtable('Planilla_voluntarios.csv');

% encuentra paciente deseado
    id = strsplit(id,'.');
    ind = strcmpi(file.Datos, id{1});

% guarda su valor de presión braquial sys y dias
    Psys = file.BrachialSYS(ind);
    Pdias = file.BrachialDIA(ind);
    Vfs = file.VFS(ind);
    Vfd = file.VFD(ind);
    Age = file.Edad(ind);

% retorna a la carpeta inicial
    cd(CurrentPath);

end