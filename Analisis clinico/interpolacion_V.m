function [Vvi] = interpolacion_V(V, Pac,fsV,fsP)
    % Se interpola la diástole para que la lngitud del ciclo coincida con
    % el la de la presión aórtica
    indV = 1:length(V);
    [~,ind_fsV] = min(V);
    V_s = V(indV <= ind_fsV);
    length(V_s)
    V_d = V(indV >= ind_fsV);
    
    ind_fsP = (ind_fsV-1)/fsV*fsP;
    indP = 1:length(Pac);
    P_s = Pac(indP <= ind_fsP);
    P_d = Pac(indP >= ind_fsP);
    
    %Interpolacion sistole
    x = linspace(0,1,length(V_s));
    x_new = linspace(0,1,length(P_s));
    V_s2 = interp1(x,V_s,x_new,'spline');
%     V_s2 = interp1(x,V_s,x_new,'linear');
    
    %Interpolacion diastole
    x = linspace(0,1,length(V_d));
    x_new = linspace(0,1,length(P_d));
    V_d2 = interp1(x,V_d,x_new,'spline');
%     V_d2 = interp1(x,V_d,x_new,'linear');
    
    Vvi = [V_s2 V_d2];
end