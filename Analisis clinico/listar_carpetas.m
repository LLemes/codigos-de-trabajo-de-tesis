% FILE:     LISTAR_CARPETAS.M
% 
% EDITED:   Lucia Lemes, May 01 2020
%           Lic. en Ingeniería Biológica, CENUR LN, UDELAR, Uruguay
% -------------------------------------------------------------------------
% INPUT:    path    ... 1xn vector, name of the directory where the search 
%                       is done
% OPTIONAL: UNIX    ... [0, 1], 0 if OS is UNIX, 1 if OS is Windows.
% 
% OUTPUT:   dir     ... 1xn vector, directories cointained in path
% 
% USAGE:    
% 
%           OUTPUT: -------------------------------------------------------
%           This routine needs a signal as an input, furthe input is
%           optional. If the directory is void, returns 0. 
%
%           MATLAB COMMAND: -----------------------------------------------
%           [dir]= emg_wavemf(path, UNIX);
%           [dir]= emg_wavemf(path); %uses default values as UNIX = 1
% -------------------------------------------------------------------------


function [d_folder, data_path] = listar_carpetas(path, UNIX)
    if nargin == 1
        UNIX=1;
    end
    
    if UNIX
        barra = '/';
    else
        barra = '\';
    end
    
    Current_path = pwd;
    if exist(['..' barra path],'dir')==7
        cd(['..' barra path]);
        data_path = pwd;        
        d = dir();
        d_folder = d([d(:).isdir]);
        d_folder = d_folder(~ismember({d_folder(:).name},{'.','..'}));
    else
        d_folder = [];
        print('Error: El directorio no existe');
    end
    cd(Current_path);
end