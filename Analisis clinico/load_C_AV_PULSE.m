function [P]=load_C_AV_PULSE(path,data,barra)
    current = pwd;
    
    cd(path);
    cd(num2str(data.ID));
    a = strcat('.', barra, data.Path_Pac);
    P = load(a{1});
    cd(current);
end