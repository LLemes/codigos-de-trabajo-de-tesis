clc;
close all;
clear all;
 
config = jsondecode(fileread('config.json'));
tabla =  readtable(config.PATH.database);
resultados = tabla(strcmp(tabla.Etiqueta,'control'),:);

for i = 1:length(resultados.ID)
%for i = [1 4 6 10 15 16 23 24 27 33]
    %i=10;
    %i=24;

    %Bucle PV

    
    % datos= [Pfs,loc_Pfs,Pfd,Vfs,Vfd,Psys,Pdias,Ea, Emax, V0, Age,fs, Co]
    [P, V, Pac, datos] = cardiac_parameters(config.PATH.pressure, resultados(i,:), config.OS.barra(config.OS.current));
    resultados.ASC(i) = datos(12);
    
    resultados.PCS(i)=max(Pac);
    resultados.PCD(i)=min(Pac);
    resultados.PCce(i)=Pac(1);
    
    resultados.Pfs(i)=datos(1);
    resultados.loc_Pfs(i)=datos(2);
    resultados.Pfd(i) = datos(3);
    resultados.R(i) = datos(4);
    resultados.C(i) = datos(5);
    resultados.Ea(i)=datos(6);
    resultados.Ea_index(i) = resultados.Ea(i)/resultados.ASC(i);
    resultados.Emax(i)=datos(7);
    resultados.Emax_index(i) = resultados.Emax(i)/resultados.ASC(i);
    resultados.V0(i)=datos(8);
    
    resultados.Co(i)=datos(9);
    resultados.Co_index(i) = resultados.Co(i)/resultados.ASC(i);
    resultados.W(i)=datos(10);
    resultados.W_index(i) = resultados.W(i)/resultados.ASC(i);
    resultados.Wdis(i)=datos(11);
    resultados.Wdis_index(i) = resultados.Wdis(i)/resultados.ASC(i);
    resultados.Wel(i)=datos(10)-datos(11);
    resultados.Wel_index(i) = resultados.Wel(i)/resultados.ASC(i);
    resultados.damping(i)= resultados.Wdis(i)/resultados.Wel(i);
    fs = resultados.fs(i);
    resultados.muestras(i) = length(Pac);
    
    t = 0:1/fs:(length(Pac)-1)/fs;
    
    % Elastancia arterial
        OrdO = resultados.Pfd(i)+(resultados.Ea(i)*resultados.VFD(i));
        vol_aux = linspace(resultados.VFD(i),resultados.VFS(i)-10,120);
        
    % Elastancia máxima simplificada
        vol_aux = linspace(0,resultados.VFD(i)+10,120);
        
    
    a=strcat(resultados.Nombre(i),{' '},resultados.Apellido(i));
    switch a{1}
        case 'Nicolas Marichal'
            V4Ca_sn = [108, 101, 99, 96, 92, 87, 83, 79, 75, 71, 67, 64, 61, 59, 53, 54, 55, 55, 53, 54, 55, 55, 55, 57, 59, 61, 64, 67, 71, 75, 79, 83, 87, 92, 96];
            V4Ca = ((resultados.VFD(i)-resultados.VFS(i))/(max(V4Ca_sn)-min(V4Ca_sn)))*V4Ca_sn + (resultados.VFS(i)-(resultados.VFD(i)-resultados.VFS(i))*min(V4Ca_sn)/(max(V4Ca_sn)-min(V4Ca_sn)));
            
            Hr = 50;
            
            fsV = 1/((60.0/Hr)/length(V4Ca));
            V4C = interpolacion_V(V4Ca,P,fsV,fs);
            dV4C = gradient(V4C)*fs;
            
            CF_grafica(t, Pac, t(resultados.loc_Pfs(i)), resultados.Pfs(i), V, P, vol_aux,(-resultados.Ea(i)*vol_aux)+OrdO, vol_aux,resultados.Emax(i)*vol_aux, V(1:length(V)-1), 0:1/fs:((length(V4C)-1)/fs), V4C/max(V4C)*resultados.VFD(i));
            
            saveas(gcf,a{1},'png');
            
            VolV = V4C(1:length(V4C)-1)';
            VolE = V(1:length(V)-1)';
            dato = table(Pac,VolV, VolE);
            name=strcat('./Datos_Leandro/dato_',a{1},'.csv');
            writetable(dato,name)
            
        case 'Gianlucca Colla'
            V4Ca_sn = [135, 134, 133, 130, 123, 115, 108, 101, 94, 88, 83, 78, 74, 71, 67, 65, 62, 61, 62, 65, 67, 71, 74, 78, 83, 88, 94, 101, 108, 115, 123, 130, 133, 134, 135];
            V4Ca = ((resultados.VFD(i)-resultados.VFS(i))/(max(V4Ca_sn)-min(V4Ca_sn)))*V4Ca_sn + (resultados.VFS(i)-(resultados.VFD(i)-resultados.VFS(i))*min(V4Ca_sn)/(max(V4Ca_sn)-min(V4Ca_sn)));
            
            Hr = 52;
            fsV = 1/((60.0/Hr)/length(V4Ca));
            V4C = interpolacion_V(V4Ca,P,fsV,fs);
            dV4C = gradient(V4C)*fs;
            
            CF_grafica(t, Pac, t(resultados.loc_Pfs(i)), resultados.Pfs(i), V, P, vol_aux,(-resultados.Ea(i)*vol_aux)+OrdO, vol_aux,resultados.Emax(i)*vol_aux, V(1:length(V)-1), 0:1/fs:(length(V4C)-1)/fs, V4C/max(V4C)*resultados.VFD(i));
            
            saveas(gcf,a{1},'png');
            
            VolV = V4C(1:length(V4C)-1)';
            VolE = V(1:length(V)-1)';
            dato = table(Pac,VolV, VolE);
            name=strcat('./Datos_Leandro/dato_',a{1},'.csv');
            writetable(dato,name)
            
        case 'Belen Aquino'
            V4Ca_sn = [79 79 77 75 72 69 66 63 60 57 54 52 49 47 45 43 42 41 40 40 39 40 42 45 49 54 58 62 63 64 64 64 65 65 65 66 66 67 67 67 68 70 71 72 72 74 76 76 79];
            V4Ca = ((resultados.VFD(i)-resultados.VFS(i))/(max(V4Ca_sn)-min(V4Ca_sn)))*V4Ca_sn + (resultados.VFS(i)-(resultados.VFD(i)-resultados.VFS(i))*min(V4Ca_sn)/(max(V4Ca_sn)-min(V4Ca_sn)));
            
            Hr = 71;
            fsV = 1/((60.0/Hr)/length(V4Ca));
            V4C = interpolacion_V(V4Ca,P,fsV,fs);
            dV4C = gradient(V4C)*fs;
            
            CF_grafica(t, Pac, t(resultados.loc_Pfs(i)), resultados.Pfs(i), V, P, vol_aux,(-resultados.Ea(i)*vol_aux)+OrdO, vol_aux,resultados.Emax(i)*vol_aux, V(1:length(V)-1), 0:1/fs:(length(V4C)-1)/fs, V4C/max(V4C)*resultados.VFD(i));
            
            saveas(gcf,a{1},'png');
            
            VolV = V4C(1:length(V4C)-1)';
            VolE = V(1:length(V)-1)';
            dato = table(Pac,VolV, VolE);
            name=strcat('./Datos_Leandro/dato_',a{1},'.csv');
            writetable(dato,name)
            
        case 'Felipe Cairus'
            V4Ca_sn = [131 131 128 123 116 109 103 97 91 86 81 76 72 68 65 63 61 60 61 62 62 63 63 65 68 72 78 83 88 93 99 103 105 105 105 105 105 106 107 107 106];
            V4Ca = ((resultados.VFD(i)-resultados.VFS(i))/(max(V4Ca_sn)-min(V4Ca_sn)))*V4Ca_sn + (resultados.VFS(i)-(resultados.VFD(i)-resultados.VFS(i))*min(V4Ca_sn)/(max(V4Ca_sn)-min(V4Ca_sn)));
            
            Hr = 49;
            fsV = 1/((60.0/Hr)/length(V4Ca));
            V4C = interpolacion_V(V4Ca,P,fsV,fs);
            dV4C = gradient(V4C)*fs;
            
            CF_grafica(t, Pac, t(resultados.loc_Pfs(i)), resultados.Pfs(i), V, P, vol_aux,(-resultados.Ea(i)*vol_aux)+OrdO, vol_aux,resultados.Emax(i)*vol_aux, V(1:length(V)-1), 0:1/fs:(length(V4C)-1)/fs, V4C/max(V4C)*resultados.VFD(i));
            
            saveas(gcf,a{1},'png');
            
            VolV = V4C(1:length(V4C)-1)';
            VolE = V(1:length(V)-1)';
            dato = table(Pac,VolV, VolE);
            name=strcat('./Datos_Leandro/dato_',a{1},'.csv');
            writetable(dato,name)
            
        case 'Facundo Mieres'
            V4Ca_sn = [108 108 107 105 100 95 89 83 78 73 69 65 61 58 55 53 51 49 49 50 52 56 61 66 71 77 82 85 87 88 89 90 91 92 92 92 92 92 92 93 94 96 98 100 101];
            V4Ca = ((resultados.VFD(i)-resultados.VFS(i))/(max(V4Ca_sn)-min(V4Ca_sn)))*V4Ca_sn + (resultados.VFS(i)-(resultados.VFD(i)-resultados.VFS(i))*min(V4Ca_sn)/(max(V4Ca_sn)-min(V4Ca_sn)));
            
            Hr = 60;
            fsV = 1/((60.0/Hr)/length(V4Ca));
            V4C = interpolacion_V(V4Ca,P,fsV,fs);
            dV4C = gradient(V4C)*fs;
            
            CF_grafica(t, Pac, t(resultados.loc_Pfs(i)), resultados.Pfs(i), V, P, vol_aux,(-resultados.Ea(i)*vol_aux)+OrdO, vol_aux,resultados.Emax(i)*vol_aux, V(1:length(V)-1), 0:1/fs:(length(V4C)-1)/fs, V4C/max(V4C)*resultados.VFD(i));
            
            saveas(gcf,a{1},'png');
            
            VolV = V4C(1:length(V4C)-1)';
            VolE = V(1:length(V)-1)';
            dato = table(Pac,VolV, VolE);
            name=strcat('./Datos_Leandro/dato_',a{1},'.csv');
            writetable(dato,name)
            
        case 'Enzo Facchin'
            V4Ca_sn = [127 126 125 120 114 108 101 96 91 86 81 77 73 69 66 63 62 63 67 74 83 93 102 108 110 110 110 110 110 110 112 116 120 123 123];
            V4Ca = ((resultados.VFD(i)-resultados.VFS(i))/(max(V4Ca_sn)-min(V4Ca_sn)))*V4Ca_sn + (resultados.VFS(i)-(resultados.VFD(i)-resultados.VFS(i))*min(V4Ca_sn)/(max(V4Ca_sn)-min(V4Ca_sn)));
            
            Hr = 81;
            fsV = 1/((60.0/Hr)/length(V4Ca));
            V4C = interpolacion_V(V4Ca,P,fsV,fs);
            dV4C = gradient(V4C)*fs;
            
            CF_grafica(t, Pac, t(resultados.loc_Pfs(i)), resultados.Pfs(i), V, P, vol_aux,(-resultados.Ea(i)*vol_aux)+OrdO, vol_aux,resultados.Emax(i)*vol_aux, V(1:length(V)-1), 0:1/fs:(length(V4C)-1)/fs, V4C/max(V4C)*resultados.VFD(i));
            
            saveas(gcf,a{1},'png');
            
            VolV = V4C(1:length(V4C)-1)';
            VolE = V(1:length(V)-1)';
            dato = table(Pac,VolV, VolE);
            name=strcat('./Datos_Leandro/dato_',a{1},'.csv');
            writetable(dato,name)
            
        case 'Mathias Bernatene'
            V4Ca_sn = [138 135 131 124 117 109 101 94 87 81 76 72 68 66 66 65 66 70 77 86 95 103 109 114 117 119 122 126 131 135 137];
            V4Ca = ((resultados.VFD(i)-resultados.VFS(i))/(max(V4Ca_sn)-min(V4Ca_sn)))*V4Ca_sn + (resultados.VFS(i)-(resultados.VFD(i)-resultados.VFS(i))*min(V4Ca_sn)/(max(V4Ca_sn)-min(V4Ca_sn)));
            
            Hr = 91;
            fsV = 1/((60.0/Hr)/length(V4Ca));
            V4C = interpolacion_V(V4Ca,P,fsV,fs);
            dV4C = gradient(V4C)*fs;
            
            CF_grafica(t, Pac, t(resultados.loc_Pfs(i)), resultados.Pfs(i), V, P, vol_aux,(-resultados.Ea(i)*vol_aux)+OrdO, vol_aux,resultados.Emax(i)*vol_aux, V(1:length(V)-1), 0:1/fs:(length(V4C)-1)/fs, V4C/max(V4C)*resultados.VFD(i));
            
            saveas(gcf,a{1},'png');
            
            VolV = V4C(1:length(V4C)-1)';
            VolE = V(1:length(V)-1)';
            dato = table(Pac,VolV, VolE);
            name=strcat('./Datos_Leandro/dato_',a{1},'.csv');
            writetable(dato,name)
            
        case 'Hugo Vera'
            V4Ca_sn = [102 101 100 99 97 93 89 84 80 75 71 68 64 61 58 56 53 52 51 53 55 56 57 57 58 59 62 65 70 75 79 83 86 89 91 93 94 94 95 95 96 97 99 100 100 101];
            V4Ca = ((resultados.VFD(i)-resultados.VFS(i))/(max(V4Ca_sn)-min(V4Ca_sn)))*V4Ca_sn + (resultados.VFS(i)-(resultados.VFD(i)-resultados.VFS(i))*min(V4Ca_sn)/(max(V4Ca_sn)-min(V4Ca_sn)));
            
            Hr = 78;
            fsV = 1/((60.0/Hr)/length(V4Ca));
            V4C = interpolacion_V(V4Ca,P,fsV,fs);
            dV4C = gradient(V4C)*fs;
            
            CF_grafica(t, Pac, t(resultados.loc_Pfs(i)), resultados.Pfs(i), V, P, vol_aux,(-resultados.Ea(i)*vol_aux)+OrdO, vol_aux,resultados.Emax(i)*vol_aux, V(1:length(V)-1), 0:1/fs:(length(V4C)-1)/fs, V4C/max(V4C)*resultados.VFD(i));
            
            saveas(gcf,a{1},'png');
            
            VolV = V4C(1:length(V4C)-1)';
            VolE = V(1:length(V)-1)';
            dato = table(Pac,VolV, VolE);
            name=strcat('./Datos_Leandro/dato_',a{1},'.csv');
            writetable(dato,name)
            
        case 'Franco Fagundez'
            V4Ca_sn = [133 130 123 114 105 96 88 81 76 71 66 63 60 59 59 60 60 62 65 71 78 85 93 100 105 109 111 111 111 112 113 114 115 116 118 121 123 124];
            V4Ca = ((resultados.VFD(i)-resultados.VFS(i))/(max(V4Ca_sn)-min(V4Ca_sn)))*V4Ca_sn + (resultados.VFS(i)-(resultados.VFD(i)-resultados.VFS(i))*min(V4Ca_sn)/(max(V4Ca_sn)-min(V4Ca_sn)));
            
            Hr = 73;
            fsV = 1/((60.0/Hr)/length(V4Ca));
            V4C = interpolacion_V(V4Ca,P,fsV,fs);
            dV4C = gradient(V4C)*fs;
            
            CF_grafica(t, Pac, t(resultados.loc_Pfs(i)), resultados.Pfs(i), V, P, vol_aux,(-resultados.Ea(i)*vol_aux)+OrdO, vol_aux,resultados.Emax(i)*vol_aux, V(1:length(V)-1), 0:1/fs:(length(V4C)-1)/fs, V4C/max(V4C)*resultados.VFD(i));
            
            saveas(gcf,a{1},'png');
            
            VolV = V4C(1:length(V4C)-1)';
            VolE = V(1:length(V)-1)';
            dato = table(Pac,VolV, VolE);
            name=strcat('./Datos_Leandro/dato_',a{1},'.csv');
            writetable(dato,name)
            
        case 'Facundo Machado'
            V4Ca_sn = [130 129 128 126 124 122 120 120 120 120 120 119 119 118 116 115 114 112 111 109 105 99 92 85 80 76 73 71 69 68 68 67 66 65 66 67 68 68 69 71 73 76 80 85 92 99 105 109 111 112 114 115 116 118 119 119 120 120 120 120 120 120 120 120 122 124 126 128 129 130 130];
            V4Ca = ((resultados.VFD(i)-resultados.VFS(i))/(max(V4Ca_sn)-min(V4Ca_sn)))*V4Ca_sn + (resultados.VFS(i)-(resultados.VFD(i)-resultados.VFS(i))*min(V4Ca_sn)/(max(V4Ca_sn)-min(V4Ca_sn)));
            
            Hr = 60;
            fsV = 1/((60.0/Hr)/length(V4Ca));
            V4C = interpolacion_V(V4Ca,P,fsV,fs);
            dV4C = gradient(V4C)*fs;
            
            CF_grafica(t, Pac, t(resultados.loc_Pfs(i)), resultados.Pfs(i), V, P, vol_aux,(-resultados.Ea(i)*vol_aux)+OrdO, vol_aux,resultados.Emax(i)*vol_aux, V(1:length(V)-1), 0:1/fs:(length(V4C)-1)/fs, V4C/max(V4C)*resultados.VFD(i));
            
            saveas(gcf,a{1},'png');
            
            VolV = V4C(1:length(V4C)-1)';
            VolE = V(1:length(V)-1)';
            dato = table(Pac,VolV, VolE);
            name=strcat('./Datos_Leandro/dato_',a{1},'.csv');
            writetable(dato,name)
            
        case 'Katerin Amaral'
            %V4Ca_sn = [70 70 69 68 67 65 62 60 57 54 52 49 46 43 41 40 38 37 35 34 33 33 32 32 34 37 42 46 51 54 57 57 58 59 60 62 64];
            V4Ca_sn = [99 98 97 95 92 89 86 85 81 77 74 68 61 54 47 43 40 38 37 34 35 36 39 41 43 45 48 50 53 55 59 62 67 71 74 76 77 79 81 82 86 89 92 95 99];
                
            V4Ca = ((resultados.VFD(i)-resultados.VFS(i))/(max(V4Ca_sn)-min(V4Ca_sn)))*V4Ca_sn + (resultados.VFS(i)-(resultados.VFD(i)-resultados.VFS(i))*min(V4Ca_sn)/(max(V4Ca_sn)-min(V4Ca_sn)));
            
            Hr = 71;
            fsV = 1/((60.0/Hr)/length(V4Ca));
            V4C = interpolacion_V(V4Ca,P,fsV,fs);
            dV4C = gradient(V4C)*fs;
            
            CF_grafica(t, Pac, t(resultados.loc_Pfs(i)), resultados.Pfs(i), V, P, vol_aux,(-resultados.Ea(i)*vol_aux)+OrdO, vol_aux,resultados.Emax(i)*vol_aux, V(1:length(V)-1), 0:1/fs:(length(V4C)-1)/fs, V4C/max(V4C)*resultados.VFD(i));
            
            saveas(gcf,a{1},'png');
            
            VolV = V4C(1:length(V4C)-1)';
            VolE = V(1:length(V)-1)';
            dato = table(Pac,VolV, VolE);
            name=strcat('./Datos_Leandro/dato_',a{1},'.csv');
            writetable(dato,name)
          
        case 'Carlos Americo'
            V4Ca_sn = [86 85 84 81 77 72 68 63 59 54 50 48 45 43 41 39 38 37 36 36 38 39 41 44 48 52 57 61 64 67 70 73 75 75 76 77 79 81 83 84];
            V4Ca = ((resultados.VFD(i)-resultados.VFS(i))/(max(V4Ca_sn)-min(V4Ca_sn)))*V4Ca_sn + (resultados.VFS(i)-(resultados.VFD(i)-resultados.VFS(i))*min(V4Ca_sn)/(max(V4Ca_sn)-min(V4Ca_sn)));
            
            Hr = 97;
            fsV = 1/((60.0/Hr)/length(V4Ca));
            V4C = interpolacion_V(V4Ca,P,fsV,fs);
            dV4C = gradient(V4C)*fs;
            
            CF_grafica(t, Pac, t(resultados.loc_Pfs(i)), resultados.Pfs(i), V, P, vol_aux,(-resultados.Ea(i)*vol_aux)+OrdO, vol_aux,resultados.Emax(i)*vol_aux, V(1:length(V)-1), 0:1/fs:(length(V4C)-1)/fs, V4C/max(V4C)*resultados.VFD(i));
            
            saveas(gcf,a{1},'png');
            
            VolV = V4C(1:length(V4C)-1)';
            VolE = V(1:length(V)-1)';
            dato = table(Pac,VolV, VolE);
            name=strcat('./Datos_Leandro/dato_',a{1},'.csv');
            writetable(dato,name)
          
        case 'Pablo Franco'
            V4Ca_sn = [78 78 77 75 73 71 69 67 65 63 62 60 57 54 51 47 43 39 37 35 33 32 32 31 31 32 32 33 35 37 39 43 47 51 54 57 60 62 63 65 67 69 71 73 75 77 78 78];
            V4Ca = ((resultados.VFD(i)-resultados.VFS(i))/(max(V4Ca_sn)-min(V4Ca_sn)))*V4Ca_sn + (resultados.VFS(i)-(resultados.VFD(i)-resultados.VFS(i))*min(V4Ca_sn)/(max(V4Ca_sn)-min(V4Ca_sn)));
            
            Hr = 83;
            fsV = 1/((60.0/Hr)/length(V4Ca));
            V4C = interpolacion_V(V4Ca,P,fsV,fs);
            dV4C = gradient(V4C)*fs;
            
            CF_grafica(t, Pac, t(resultados.loc_Pfs(i)), resultados.Pfs(i), V, P, vol_aux,(-resultados.Ea(i)*vol_aux)+OrdO, vol_aux,resultados.Emax(i)*vol_aux, V(1:length(V)-1), 0:1/fs:(length(V4C)-1)/fs, V4C/max(V4C)*resultados.VFD(i));
            
            saveas(gcf,a{1},'png');
            
            VolV = V4C(1:length(V4C)-1)';
            VolE = V(1:length(V)-1)';
            dato = table(Pac,VolV, VolE);
            name=strcat('./Datos_Leandro/dato_',a{1},'.csv');
            writetable(dato,name)
          
        case 'Sol Castro'
            V4Ca_sn = [101 99 99 97 95 92 90 89 88 87 87 86 85 84 83 81 80 78 76 73 69 65 61 56 52 47 44 42 40 39 40 41 44 47 52 56 61 65 69 73 76 78 80 81 83 84 85 86 87 88 89 90 92 95 97 99 101];
            V4Ca = ((resultados.VFD(i)-resultados.VFS(i))/(max(V4Ca_sn)-min(V4Ca_sn)))*V4Ca_sn + (resultados.VFS(i)-(resultados.VFD(i)-resultados.VFS(i))*min(V4Ca_sn)/(max(V4Ca_sn)-min(V4Ca_sn)));
            
            Hr = 49;
            fsV = 1/((60.0/Hr)/length(V4Ca));
            V4C = interpolacion_V(V4Ca,P,fsV,fs);
            dV4C = gradient(V4C)*fs;
            
            CF_grafica(t, Pac, t(resultados.loc_Pfs(i)), resultados.Pfs(i), V, P, vol_aux,(-resultados.Ea(i)*vol_aux)+OrdO, vol_aux,resultados.Emax(i)*vol_aux, V(1:length(V)-1), 0:1/fs:(length(V4C)-1)/fs, V4C/max(V4C)*resultados.VFD(i));
            
            saveas(gcf,a{1},'png');
            
            VolV = V4C(1:length(V4C)-1)';
            VolE = V(1:length(V)-1)';
            dato = table(Pac,VolV, VolE);
            name=strcat('./Datos_Leandro/dato_',a{1},'.csv');
            writetable(dato,name)
            
        case 'Fernando Martinez'
            %V4Ca_sn = [104 103 101 98 94 91 88 87 86 86 85 85 85 84 83 80 75 70 65 60 55 51 48 46 45 44 44 45 47 49 51 53 55 58 61 64 67 71 75 80 84 88 91 94 95 95];
            V4Ca_sn = [99 98 96 93 89 86 83 80 73 68 62 57 52 47 44 43 42 41 42 44 46 48 50 52 54 57 60 63 66 71 75 80 84 87 89 90];
            V4Ca = ((resultados.VFD(i)-resultados.VFS(i))/(max(V4Ca_sn)-min(V4Ca_sn)))*V4Ca_sn + (resultados.VFS(i)-(resultados.VFD(i)-resultados.VFS(i))*min(V4Ca_sn)/(max(V4Ca_sn)-min(V4Ca_sn)));
            
            Hr = 70;
            fsV = 1/((60.0/Hr)/length(V4Ca));
            V4C = interpolacion_V(V4Ca,P,fsV,fs);
            dV4C = gradient(V4C)*fs;
            
            CF_grafica(t, Pac, t(resultados.loc_Pfs(i)), resultados.Pfs(i), V, P, vol_aux,(-resultados.Ea(i)*vol_aux)+OrdO, vol_aux,resultados.Emax(i)*vol_aux, V(1:length(V)-1), 0:1/fs:(length(V4C)-1)/fs, V4C/max(V4C)*resultados.VFD(i));
            
            saveas(gcf,a{1},'png');
            
            VolV = V4C(1:length(V4C)-1)';
            VolE = V(1:length(V)-1)';
            dato = table(Pac,VolV, VolE);
            name=strcat('./Datos_Leandro/dato_',a{1},'.csv');
            writetable(dato,name)
                        
        case 'Horacio Reyes'
            V4Ca_sn = [120 118 115 113 113 112 110 107 103 98 93 89 84 80 76 72 68 64 61 59 57 55 53 53 54 55 58 62 67 74 79 83 87 90 93 95 96 98 99 99 99 100 100 101 102 103 104 105 108 111 114 116 117 118];
            V4Ca = ((resultados.VFD(i)-resultados.VFS(i))/(max(V4Ca_sn)-min(V4Ca_sn)))*V4Ca_sn + (resultados.VFS(i)-(resultados.VFD(i)-resultados.VFS(i))*min(V4Ca_sn)/(max(V4Ca_sn)-min(V4Ca_sn)));
            
            Hr = 68;
            fsV = 1/((60.0/Hr)/length(V4Ca));
            V4C = interpolacion_V(V4Ca,P,fsV,fs);
            dV4C = gradient(V4C)*fs;
            
            CF_grafica(t, Pac, t(resultados.loc_Pfs(i)), resultados.Pfs(i), V, P, vol_aux,(-resultados.Ea(i)*vol_aux)+OrdO, vol_aux,resultados.Emax(i)*vol_aux, V(1:length(V)-1), 0:1/fs:(length(V4C)-1)/fs, V4C/max(V4C)*resultados.VFD(i));
            
            saveas(gcf,a{1},'png');
            
            VolV = V4C(1:length(V4C)-1)';
            VolE = V(1:length(V)-1)';
            dato = table(Pac,VolV, VolE);
            name=strcat('./Datos_Leandro/dato_',a{1},'.csv');
            writetable(dato,name)
                        
        case 'Santiago Curcio'
            V4Ca_sn = [83 81 78 76 74 72 71 71 71 67 62 57 53 49 45 42 40 39 39 38 38 39 40 42 45 49 53 57 62 67 71 74 76 76 76 78 81 83];
            V4Ca = ((resultados.VFD(i)-resultados.VFS(i))/(max(V4Ca_sn)-min(V4Ca_sn)))*V4Ca_sn + (resultados.VFS(i)-(resultados.VFD(i)-resultados.VFS(i))*min(V4Ca_sn)/(max(V4Ca_sn)-min(V4Ca_sn)));
            
            Hr = 63;
            fsV = 1/((60.0/Hr)/length(V4Ca));
            V4C = interpolacion_V(V4Ca,P,fsV,fs);
            dV4C = gradient(V4C)*fs;
            
            CF_grafica(t, Pac, t(resultados.loc_Pfs(i)), resultados.Pfs(i), V, P, vol_aux,(-resultados.Ea(i)*vol_aux)+OrdO, vol_aux,resultados.Emax(i)*vol_aux, V(1:length(V)-1), 0:1/fs:(length(V4C)-1)/fs, V4C/max(V4C)*resultados.VFD(i));
            
            saveas(gcf,a{1},'png');
            
            VolV = V4C(1:length(V4C)-1)';
            VolE = V(1:length(V)-1)';
            dato = table(Pac,VolV, VolE);
            name=strcat('./Datos_Leandro/dato_',a{1},'.csv');
            writetable(dato,name)
                        
        case 'Luis Rio'
            %V4Ca_sn = [100 99 97 95 92 88 84 79 74 70 65 61 57 53 50 47 44 42 41 40 39 39 39 40 41 43 45 48 51 55 59 62 64 66 67 68 69 70 70 70 72 75 77 79 81 84 88 92 95 97 99 100];
            V4Ca_sn = [101 99 97 95 92 89 84 80 75 71 66 62 58 54 51 48 45 43 41 40 39 39 40 41 43 45 48 51 54 58 62 66 71 75 80 84 89 92 95 97 99 101];
            V4Ca = ((resultados.VFD(i)-resultados.VFS(i))/(max(V4Ca_sn)-min(V4Ca_sn)))*V4Ca_sn + (resultados.VFS(i)-(resultados.VFD(i)-resultados.VFS(i))*min(V4Ca_sn)/(max(V4Ca_sn)-min(V4Ca_sn)));
            
            Hr = 83;
            fsV = 1/((60.0/Hr)/length(V4Ca));
            V4C = interpolacion_V(V4Ca,P,fsV,fs);
            dV4C = gradient(V4C)*fs;
            
            CF_grafica(t, Pac, t(resultados.loc_Pfs(i)), resultados.Pfs(i), V, P, vol_aux,(-resultados.Ea(i)*vol_aux)+OrdO, vol_aux,resultados.Emax(i)*vol_aux, V(1:length(V)-1), 0:1/fs:(length(V4C)-1)/fs, V4C/max(V4C)*resultados.VFD(i));
            
            saveas(gcf,a{1},'png');
            
            VolV = V4C(1:length(V4C)-1)';
            VolE = V(1:length(V)-1)';
            dato = table(Pac,VolV, VolE);
            name=strcat('./Datos_Leandro/dato_',a{1},'.csv');
            writetable(dato,name)
                 
        case 'Leticia Sarute'
            V4Ca_sn = [72 71 69 66 62 58 54 50 46 43 39 36 33 31 29 27 26 25 25 24 24 25 26 27 29 31 33 36 39 43 46 50 54 58 62 66 69 71 72 72];
            V4Ca = ((resultados.VFD(i)-resultados.VFS(i))/(max(V4Ca_sn)-min(V4Ca_sn)))*V4Ca_sn + (resultados.VFS(i)-(resultados.VFD(i)-resultados.VFS(i))*min(V4Ca_sn)/(max(V4Ca_sn)-min(V4Ca_sn)));
            
            Hr = 91;
            fsV = 1/((60.0/Hr)/length(V4Ca));
            V4C = interpolacion_V(V4Ca,P,fsV,fs);
            dV4C = gradient(V4C)*fs;
            
            CF_grafica(t, Pac, t(resultados.loc_Pfs(i)), resultados.Pfs(i), V, P, vol_aux,(-resultados.Ea(i)*vol_aux)+OrdO, vol_aux,resultados.Emax(i)*vol_aux, V(1:length(V)-1), 0:1/fs:(length(V4C)-1)/fs, V4C/max(V4C)*resultados.VFD(i));
            
            saveas(gcf,a{1},'png');
            
            VolV = V4C(1:length(V4C)-1)';
            VolE = V(1:length(V)-1)';
            dato = table(Pac,VolV, VolE);
            name=strcat('./Datos_Leandro/dato_',a{1},'.csv');
            writetable(dato,name)
            
        case 'Valentina Reyes'
            V4Ca_sn = [67 66 64 63 61 59 58 57 56 56 55 55 55 54 52 50 47 44 41 38 35 33 32 32 32 35 38 41 44 47 50 52 54 55 55 56 56 57 58 59 61 63 64 66 67];
            V4Ca = ((resultados.VFD(i)-resultados.VFS(i))/(max(V4Ca_sn)-min(V4Ca_sn)))*V4Ca_sn + (resultados.VFS(i)-(resultados.VFD(i)-resultados.VFS(i))*min(V4Ca_sn)/(max(V4Ca_sn)-min(V4Ca_sn)));
            
            Hr = 70;
            fsV = 1/((60.0/Hr)/length(V4Ca));
            V4C = interpolacion_V(V4Ca,P,fsV,fs);
            dV4C = gradient(V4C)*fs;
            
            CF_grafica(t, Pac, t(resultados.loc_Pfs(i)), resultados.Pfs(i), V, P, vol_aux,(-resultados.Ea(i)*vol_aux)+OrdO, vol_aux,resultados.Emax(i)*vol_aux, V(1:length(V)-1), 0:1/fs:(length(V4C)-1)/fs, V4C/max(V4C)*resultados.VFD(i));
            
            saveas(gcf,a{1},'png');
            
            VolV = V4C(1:length(V4C)-1)';
            VolE = V(1:length(V)-1)';
            dato = table(Pac,VolV, VolE);
            name=strcat('./Datos_Leandro/dato_',a{1},'.csv');
            writetable(dato,name)
            
    end
%     xlabel('Tiempo [s]');ylabel('Volumen estimado [ml]');
%     ylim([min(V)-10,max(V)+10]);

%     
%     figure()
%     plot(0:1/fsV:(length(V4Ca)-1)/fsV,V4Ca,'o')
%     hold on
%     plot(0:1/fs:(length(V4C)-1)/fs,V4C,'*')
%     title(aux(1));
% 
% 
%     figure()
%     plot(V, P);
%     hold on
%     [~,k] = min(V4C);
%     P_sp = Pac(1:k,1);
%     P_sd = ones((length(Pac)-length(P_sp)),1)*Pfd;
%     P_new = [P_sp; P_sd; P_sp(1)];
%     plot(V4C,P_new);
%     %Elastancia arterial
%         OrdO = Pfd+(Ea*Vfd);
%         vol_aux = linspace(Vfd,Vfs-10,120);
%         plot(vol_aux,(-Ea*vol_aux)+OrdO)
%     %Elastancia máxima simplificada
%         vol_aux = linspace(0,Vfd+10,120);
%         plot(vol_aux,Emax*vol_aux);
%     hold off
%     xlabel('Volumen (ml)'); ylabel('Presión (mmHg)'); legend('Bucle PV (V estimado)','Bucle PV (V real, P reestimado)','Recta Ea', 'Recta Emax');
%     title(aux(1));
% 
%     figure()
%     plot(P,'o')
%     hold on
%     plot(Pac)
%     plot(P_new)
%     hold off
%     title(aux(1));
    
%     texto = [strcat("Presión de fin de sístole: ", num2str(Pfs),"mmHg"), strcat("Tiempo de fin de sístole: ", num2str(t(loc_Pfs)), "s"), strcat("Relación con PBS: Pfs=", relacion, "PBS")," ", "Presiones de fin de sístole estimadas:",strcat("   Pfs=0.9*PBS=", num2str(0.9*Psys), "mmHg"), strcat("   Pfs=(2*PBS+PBD)/3=",num2str((2*Psys + Pdias)/3.0), "mmHg")," ", "Otros parámetros:", strcat("   Pfd = ",num2str(Pfd)), strcat("   Emax = ",num2str(Emax)), strcat("   Ea = ",num2str(Ea))];
%     annotation('textbox', [0.53, 0.15, 0.33, 0.3], 'string', texto )

end

name=strcat('resultados_',unique(resultados.Etiqueta),'.csv');
writetable(resultados,name{1})