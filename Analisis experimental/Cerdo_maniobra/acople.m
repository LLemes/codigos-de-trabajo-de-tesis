function [ind_S, Eap, Evi] = acople(t, P, V, ind_FD, ind_ES, E_ES, V0, P_ISO, ind_CE)
    P_CE = P(ind_CE);
    V_CE = V(ind_CE);

    % Cálculo de Eap y Evi ................................................
    ind_S = (ind_CE + 1):ind_ES;
    Eap = zeros(1,length(ind_S));
    Evi = zeros(1,length(ind_S));

    for i = ind_S
        Eap(i-ind_CE) = (P_CE - P(i))/(V_CE - V(i));
        Evi(i-ind_CE) = (P(i))/(V(i)-V0);
    end

    ind_valido = Eap<0;

    figure()
    plot(abs(Eap(ind_valido)))
    hold on
    plot(Evi(ind_valido))
    ylim([0 max(Evi)+3])
    hold off

    figure()
    plot(P(ind_S.*ind_valido),abs(Eap(ind_valido)))
    hold on
    plot(P(ind_S.*ind_valido),Evi(ind_valido))
    ylim([0 max(Evi)+3])
    hold off

    % Valor de P? .........................................................
    inters = abs(Eap) <= Evi;
    aux_IG = find(inters.*ind_S,1,'first');
    ind_IG = ind_S(aux_IG);

    figure()
    plot(t,P)
    hold on
    plot(t(ind_ES),P(ind_ES),'bo')
    plot(t(ind_CE),P(ind_CE),'ro')
    plot(t(ind_IG),P(ind_IG),'go')
    hold off
    legend('P_{LV}', ['P_{FS} =' num2str(P(ind_ES))],['P_{CE} =' num2str(P(ind_CE))],['P_? =' num2str(P(ind_IG))]);
     
    figure()
    plot(V,P)
    hold on
    
    V_ES=V(ind_ES);
    P_ES=P(ind_ES);
    k = P_ES - (E_ES*V_ES);
    
    plot(V_ES,P_ES,'bo')
    plot(V_CE,P_CE,'ro')
    plot(V(ind_IG),P(ind_IG),'go')
    plot([V0, V(1)],E_ES*[V0, V(1)] + k)

    plot(V_ES,P_ES,'o')
    plot(V(ind_FD),P_ISO,'o')
    plot([V0, V(ind_FD)],E_ES*[V0, V(ind_FD)] + k)
    plot(V(ind_FD),P(ind_FD),'o')
    hold off

    legend('Bucle PV_{LV}', 'Fin de sístole','Comienzo de eyección','Punto de intersección','Recta de E_{ES}');

end