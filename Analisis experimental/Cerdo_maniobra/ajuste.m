function [Emax,V0, Vm,Pm, e] = ajuste(P, V, V0)
    e = zeros(size(P));
    Emax = zeros(1,length(P(:,1)));
    Pm = zeros(1,length(P(:,1)));
    Vm = zeros(1,length(P(:,1)));
        
    % Para cada ciclo
    for i=1:length(P(:,1))
        % Calculo Ei
        e(i,:) = P(i,:)./(V(i,:)-V0);
        e = e-min(e);
        % Calculo Emax
        [Emax(i),ind_Emax] = max(e(i,:));
        Pm(i) = P(i,ind_Emax);
        Vm(i) = V(i,ind_Emax);
    end

    % Regresión lineal
    ind2 = Pm>(0.05*max(Pm));
    p=polyfit(Vm(ind2),Pm(ind2),1);

    % Actualizo valores
    Emax = p(1);
    V0 = -1*p(2)/Emax;
end