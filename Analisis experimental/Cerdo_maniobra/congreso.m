figure()

subplot(221)

yyaxis left
plot(t(j,:),P(j,:),'-', 'LineWidth',1);
hold on
plot(t(j,ind_CEj:ind_ESj),P(j,ind_CEj:ind_ESj),'-', 'LineWidth',2.5)
ylim([min(P(j,:)) max(P(j,:))])
ylabel('P_{LV} [mmHg]')

yyaxis right
plot(t(j,:),V(j,:),'-', 'LineWidth',1);
hold on
plot(t(j,ind_CEj:ind_ESj),V(j,ind_CEj:ind_ESj), '-','LineWidth',2.5)
hold off
xlim([min(t(j,:)) max(t(j,:))])
ylim([min(V(j,:)) max(V(j,:))])

ylabel('V_{LV} [ml]')
xlabel('t [s]')

subplot(223)

% Qstored(ind_ESj+1:length(t(j,:))) = Q_modelo(ind_ESj+1:length(t(j,:)),2) - (Pao_true(ind_ESj+1:length(t(j,:)),2))/(Rp);
% Qstored(ind_CEj:ind_ESj) = Q_modelo(ind_CEj:ind_ESj,2)
% %Qstored = QLV.signals.values - (Pao2/(Rp));
% Ts = Q_modelo(2,1)-Q_modelo(1,1);
% 
% AoEDV = 5;
% VAo = AoEDV+cumtrapz(Qstored)*Ts;

P_CE = P(j,1);
V_CE = V(j,1);

VAo_aux = V(j,:) - V_CE;
VAo_aux = -VAo_aux + V_CE;
VAo_s= VAo_aux(ind_CEj:ind_ESj);
descenso_V = fit([t_aux(1); t_aux(length(t_aux))],[VAo_aux(ind_ESj); VAo_aux(ind_CEj)], 'exp1');

VAo_d = descenso_V.a*exp(descenso_V.b*t_aux);
VAo = [VAo_s VAo_d(2:length(VAo_d))]

yyaxis left
plot(Pao_true(:,1),Pao_true(:,2),'-', 'LineWidth',1);
hold on
plot(t(j,ind_CEj:ind_ESj),P(j,ind_CEj:ind_ESj),'-', 'LineWidth',2.5)
ylim([min(Pao_true(:,2)) max(Pao_true(:,2))])
ylabel('P_{Ao} [mmHg]')

yyaxis right
plot(t(j,:),VAo,'-', 'LineWidth',1);
hold on
plot(t(j,ind_CEj:ind_ESj),VAo(ind_CEj:ind_ESj), '-','LineWidth',2.5)
hold off
xlim([min(t(j,:)) max(t(j,:))])
ylim([min(VAo) max(VAo)])

ylabel('V_{Ao} [ml]')
xlabel('t [s]')

subplot(122)

Pao_CE = Pao_true(1,2);
Vao_CE = VAo(1);

plot(V(j,:),P(j,:),'b-', 'LineWidth',2)
hold on
% Flip bucle aortico
plot(V_CE-(VAo-Vao_CE),Pao_true(:,2),  'm-','LineWidth',2)
plot(VAo-V_CE+V(j,ind_ESj),Pao_true(:,2),  'm--','LineWidth',2)

plot(V_CE,Pao_CE,'o','MarkerFaceColor','g')
plot(V(j,ind_ESj),P(j,ind_ESj),'o','MarkerFaceColor','g')
hold off
% plot(V_FS-(VAo-Vao_FS),Pao2)
xlabel('Volume [ml]')
ylabel('Pressure [mmHg]')
legend('LV PV-Loop','Reflected Aortic PV-Loop','Aortic PV-Loop')
