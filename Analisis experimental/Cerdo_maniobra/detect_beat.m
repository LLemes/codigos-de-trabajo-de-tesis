function [ind_CE, ind_FD] = detect_beat(S,dt)
    dis = (1/dt)/(250/60);
    dS = gradient(S);
    [pk,indM] = findpeaks(dS, 'MinPeakHeight',0.2*max(dS)+mean(dS),'MinPeakDistance', dis);
    
    %umbral
    u = mean(pk)/4;
    ind_FD = [];
    for i=2:length(dS)
        if (dS(i)>=u)&&(dS(i-1)<u)
            ind_FD = [ind_FD i];
        end
    end
    ind_FD = ind_FD(2:length(ind_FD));
    
    
    % Encontramos el punto de comienzo de eyección ............................
    % pico negativo de d2P/dt2 
    d2S = gradient(dS);
    [~,indc] = findpeaks(S(ind_FD(1):length(S)), 'MinPeakHeight',0.2*max(S)+mean(S),'MinPeakDistance', dis);
    indc = indc+ind_FD(1);
    ind_CE = zeros(1,length(ind_FD));
    for i=1:length(ind_FD)
        [~, ind_CE(i)] = min(d2S(ind_FD(i):indc(i)));
        ind_CE(i)=ind_CE(i)+ind_FD(i);
    end
    
%     figure()
%     subplot(3,1,1)
%     plot(S,'LineWidth', 1.5)
%     hold on
%     plot(ind_CE,S(ind_CE),'o','MarkerFaceColor','r')
%     plot(ind_FD,S(ind_FD),'o','MarkerFaceColor','g')
%     plot(indM,S(indM),'o','MarkerFaceColor','k')
%     hold off
%     ylabel('Presión (mmHg)')
%     subplot(3,1,2)
%     plot(dS,'LineWidth', 1.5)
%     hold on
%     plot(ind_CE,dS(ind_CE),'o','MarkerFaceColor','r')
%     plot(indM,dS(indM),'o','MarkerFaceColor','r')
%     plot(ind_FD,dS(ind_FD),'o','MarkerFaceColor','r')
%     hold off
%     ylabel('puntos de FD')
    
end