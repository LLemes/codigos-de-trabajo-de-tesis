function Sc = filtradoMM(S,L)
    Sc = S;
    for i=L+1:length(S)
        Sc(i) = 0;
        for j=0:L-1
            Sc(i) = Sc(i) + (S(i-j)/L);
        end
    end
end