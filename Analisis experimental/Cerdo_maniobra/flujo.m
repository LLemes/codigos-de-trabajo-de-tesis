function Q_in = flujo(V,dt, ind_CE, ind_ES)
% Calculo de Flujo

    Q = -gradient(V)/dt;
    Q_in = zeros(1,length(Q));
    Q_in(ind_CE:ind_ES) = Q(ind_CE:ind_ES);
    Q_in(ind_CE) = 0;
    
    figure()
    plot(Q)
    hold on
    plot(Q_in)
    
    N=4;
    Q_in = filtfilt(ones(1,N)/N,1,Q_in);

    Q_in = (Q_in*max(Q))/max(Q_in);

    plot(Q_in)
    hold off
end