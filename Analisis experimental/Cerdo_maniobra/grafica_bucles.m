function fig = grafica_bucles(P, V, VAo, Pao)
    P_CE = P(1);
    V_CE = V(1);

    Pao_CE = Pao(1);
    Vao_CE = VAo(1);


    fig = figure(300);
    hold on
    % Flip bucle aortico
    plot(V_CE-(VAo-Vao_CE),Pao)
    plot(V,P)
    plot(V_CE,P_CE,'o')
    plot(V_CE,Pao_CE,'o')
    hold off
    % plot(V_FS-(VAo-Vao_FS),Pao2)
    xlabel('Volumen (ml)')
    ylabel('Presión (mmHg)')
    title('Bucle PV arterial')
    legend('Bucle PV arterial','Bucle PV estimado')
end