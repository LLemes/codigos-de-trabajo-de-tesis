clc
close all

%%
FILE = 'file_info.csv';
opts = detectImportOptions(strcat(['Datos_cerdo/',FILE]));
T = readtable(strcat(['Datos_cerdo/',FILE]));

indf = strcmp(T.occlusion,'True');

Res = table();
Res.filename = T.filename(indf);
Res.injection = T.injection_count(indf);
%%
for i = 1:1%length(Res.injection)
    opts = detectImportOptions(strcat(['Datos_cerdo/',Res.filename{i}]));

    Tm = readtable(strcat(['Datos_cerdo/',Res.filename{i}]));

    a = split(Tm.tstamp(1:2),':');
    dt = str2double(a{2,length(a)}) - str2double(a{1,length(a)});

    %% Análisis de latido único
    % Defino las variables a usar .............................................
    Pm = Tm.LVP;
    Vm = Tm.LVV;
    tm = 0:dt:dt*(length(Pm)-1);

    % Detección de ciclos .....................................................
    [ind_CEm, ind_FDm] = detect_beat(Pm, dt);
    % ind = Valentinuzzi(Pm, dt, bpFilt);
    ind_FDm = ind_FDm - 10;

    %% Separación de ciclos ...................................................
    t = cut_beat(tm,ind_FDm,1);
    P = cut_beat(Pm,ind_FDm,1);
    V = cut_beat(Vm,ind_FDm,1);

    j=1;

    %% Analisis de latido unico ...............................................
    [ind_FD, ind_ES, E_ES, V0, P_ISO] = latido_unico(t(j,:), P(j,:), V(j,:));

    %% Elastancia aparente ....................................................
    ind_CE = ind_CEm(j)-ind_FDm(j);
    [ind_S, Eap, Evi] = acople(t(j,:), P(j,:), V(j,:), ind_FD, ind_ES, E_ES, V0, P_ISO, ind_CE);

    %% Guardo parametros
    Res.E_ES(i) = E_ES;
    Res.V0(i) = V0;
    Res.P_ISO(i) = P_ISO;
     %% Flujo
    % Q_in = flujo(V(j,:),dt, ind_CE, ind_ES);
    % 
    % % Presion aortica sistolica
    % Pao_s = P(j,ind_CE:ind_ES);
    % Pao_t = t(j,ind_CE:ind_ES);
    % Pao_true = zeros(length(Pao_s),2);
    % Pao_true(:,1)=Pao_t;
    % Pao_true(:,2)=Pao_s;
    % 
    % % Flujo final
    %     completo = false;
    %     
    %     if completo
    %         Q_modelo = zeros(length(V(j,:)),2);
    %         Q_modelo(:,1)=t(j,:);
    %         Q_modelo(:,2)=Q_in;
    %     else
    %         % Flujo
    %         Q_modelo = zeros(length(Pao_t),2);
    %         Q_modelo(:,1)=Pao_t;
    %         Q_modelo(:,2)=Q_in(ind_CE:ind_ES);
    %     end
    % 
    % %% Estimación Pao
    % PAo_CE = P(j,ind_CE);
    % 
    % % Parametros WindKessel
    %     % Rp [mmHg.s/ml]
    %     % L [mmHg.s2/ml]
    %     % Ra [mmHg.s/ml]
    %     % C [ml/mmHg]
    % % Valores ajustados
    % C = 8.6998;
    % L = 0.47535;
    % Ra = 0.014278;
    % Rp = 0.81267;
    % Rw = 0.013749;
    % 
    % 
    % %% LLamado modelo simulink
    % 
    % mdl = 'WK4p';
    % open_system(mdl);
    % in = Simulink.SimulationInput(mdl);
    % out = sim(in);
    % 
    % QLV = out.QLV;
    % Pao = out.Pao;
    % %% Flujo aórtico
    % 
    % Qstored = QLV.signals.values - (Pao.signals.values/(Rp));
    % %Qstored = QLV.signals.values - (Pao2/(Rp));
    % Ts = QLV.time(2)-QLV.time(1);
    % 
    % AoEDV = 5;
    % VAo = AoEDV+cumtrapz(Qstored)*Ts;
    % 
    % EAo = Pao.signals.values ./ VAo;
    % 
    % %% Visualizacion-----------------------------------------------------------
    % 
    % figure()
    % plot(t(j,:),P(j,:))
    % hold on
    % plot(Pao_t,Pao_s)
    % plot(Pao.time, Pao.signals.values)
    % hold off
    % legend('P_{LV}', 'P_{ao}','P_{ao_{WK}}');
    % 
    % figure()
    % plot(QLV.time,QLV.signals.values)
    % hold on
    % plot(QLV.time,Qstored)
    % hold off
    % legend('Q_{LV}', 'Q_{ao_{WK}}');
    % 
    % 
    % % Graficar bucles superpuestos
    % fig = grafica_bucles(t, P, V, dt, VAo, Pao, ind_CE, j);
end
%%
name=strcat('resultados_prelim.csv');
writetable(Res,name)