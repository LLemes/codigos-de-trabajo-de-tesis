clc

%%
filename = 'NOV08010.csv';
opts = detectImportOptions(strcat(['Datos_cerdo/',filename]));

Tm = readtable(strcat(['Datos_cerdo/',filename]));

a = split(Tm.tstamp(1:2),':');
dt = str2double(a{2,length(a)}) - str2double(a{1,length(a)});

%% Estimacion de Parterial
% Defino las variables a usar .............................................
Pm = Tm.LVP;
Vm = Tm.LVV;
tm = 0:dt:dt*(length(Pm)-1);

% Detección de ciclos .....................................................
[ind_CEm, ind_FDm] = detect_beat(Pm, dt);

%% Separación de ciclos ...................................................
t = cut_beat(tm,ind_CEm,1);
P = cut_beat(Pm,ind_CEm,1);
V = cut_beat(Vm,ind_CEm,1);

j=1

%% Calculo de ES
E_vij = P(j,:)./(V(j,:)-V0);
[E_ESj,ind_ESj]=max(E_vij);

ind_CEj = 1;

%% Flujo
Q_in = flujo(V(j,:),dt, ind_CEj, ind_ESj);

% Presion aortica sistolica
Pao_s = P(j,ind_CEj:ind_ESj);
x = length(t(j,:))-length(Pao_s);
t_aux = [t(j,ind_ESj:length(t(j,:)))];
% t_aux = [t(j,ind_ESj:length(t(j,:))) (t(j,2:ind_CEj)-t(j,1)+t(j,length(t(j,:))))];
t_aux = t_aux - t_aux(1);
descenso = fit([0; t_aux(length(t_aux)-1:length(t_aux))'],[P(j,ind_ESj); P(j,ind_CEj)+0.01; P(j,ind_CEj) ], 'a*exp(b*x)+c','StartPoint',[1,-0.1,P(j,ind_CEj)]);

Pao_d =descenso.a*exp(descenso.b*t_aux)+descenso.c;

Pao_true = zeros(length(Pao_s)+length(Pao_d)-1,2);
Pao_true(:,1)=t(j,:);
Pao_true(:,2)=[Pao_s Pao_d(2:length(Pao_d))];

% Flujo final
Q_modelo = zeros(length(V(j,:)),2);
Q_modelo(:,1)=t(j,:);
Q_modelo(:,2)=Q_in;

% Pao_true = zeros(length(Pao_s),2);
% Pao_true(:,1)= t(j,1:ind_ESj);
% Pao_true(:,2)= Pao_s;
% 
% % Flujo final
% Q_modelo = zeros(length(V(j,1:ind_ESj)),2);
% Q_modelo(:,1)= t(j,1:ind_ESj);
% Q_modelo(:,2)= Q_in(1:ind_ESj);


%% Estimación Pao
PAo_CE = P(j,ind_CEj);

% Parametros WindKessel
    % Rp [mmHg.s/ml]
    % L [mmHg.s2/ml]
    % Ra [mmHg.s/ml]
    % C [ml/mmHg]
% Valores ajustados
% C = 8.2335;
% L = 0.33377;
% Ra = 0.011995;
% Rp = 0.99888;
% Rw = 0.011534;

% C = 4.6395;
% L = 3.9224;
% Ra = 0.009718;
% Rp = 0.95505;
% Rw = 0.0069397;

C = 4.6232;
L = 5.0757;
Ra = 0.010502;
Rp = 0.96265;
Rw = 0.0066672;

% C = 4.2701;
% L = 0.00028079;
% Ra = 0.11883;
% Rp = 1.0002;
% Rw = 0.018303;

%% LLamado modelo simulink
mdl = 'WK5';
open_system(mdl);
%in = Simulink.SimulationInput(mdl);
sim(mdl,t(j,:));

% QLV = out.QLV;
% Pao = out.Pao;
%% Flujo aórtico

ind = QLV.signals.values>0;
ind(1) = 1;

Q_perdido = -(Pao.signals.values/(Rp));
A = trapz(Pao.time, Q_perdido)/trapz(Pao.time(~ind), Q_perdido(~ind));
A=1;
Qstored(~ind) = QLV.signals.values(~ind) - A*(Pao.signals.values(~ind)/(Rp));
Qstored(ind) = QLV.signals.values(ind);
%Qstored = QLV.signals.values - (Pao2/(Rp));
Ts = QLV.time(2)-QLV.time(1);

AoEDV = 5;
VAo = AoEDV+cumtrapz(Qstored)*Ts;

EAo = Pao.signals.values ./ VAo;

%% Visualizacion-----------------------------------------------------------

figure()
plot(t(j,:),P(j,:))
hold on
plot(Pao_true(:,1),Pao_true(:,2))
plot(Pao.time, Pao.signals.values)
hold off
legend('P_{LV}', 'P_{ao}','P_{ao_{WK}}');

figure()
plot(QLV.time,QLV.signals.values)
hold on
plot(QLV.time,Qstored)
hold off
legend('Q_{LV}', 'Q_{ao_{WK}}');


% Graficar bucles superpuestos
fig = grafica_bucles(P(j,:), V(j,:), VAo, Pao.signals.values);


% figure()
% subplot(221)
% subplot(222)
% subplot(223)
% subplot(224)
