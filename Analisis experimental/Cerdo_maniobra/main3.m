clc
close all

%%

C = 4.5723;
L = 0.017382;
Ra = 0.017768;
Rp = 0.84642;
Rw = 0.0032922;

filename = 'NOV08010.csv';
opts = detectImportOptions(strcat(['Datos_cerdo/',filename]));

Tm = readtable(strcat(['Datos_cerdo/',filename]));

a = split(Tm.tstamp(1:2),':');
dt = str2double(a{2,length(a)}) - str2double(a{1,length(a)});

%% Estimacion de Parterial
% Defino las variables a usar .............................................
Pm = Tm.LVP;
Vm = Tm.LVV;
tm = 0:dt:dt*(length(Pm)-1);
% 
% C = 1.6172;
% L = 0.050733;
% Ra = 0.014863;
% Rp = 1.8525;
% Rw = 0.0179;
% 
% i=1;
% filename = strcat(['Datos PV Edward/Datos PV ',num2str(i),'.xlsx']);
% opts = detectImportOptions(filename);
% opts.VariableUnitsRange = 'A3';
% opts.VariableTypes = {'double', 'double', 'double', 'double', 'double', 'double', 'double', 'double', 'double', 'double'};
% 
% Tm = readtable(filename, opts);
% dt = Tm.Time(2)-Tm.Time(1);
% 
% %% Estimacion de Parterial
% % Defino las variables a usar .............................................
% Pm = Tm.LVPressure;
% Vm = Tm.LVTotalVolume;
% tm = 0:dt:dt*(length(Pm)-1);

% Detección de ciclos .....................................................
[ind_CEm, ind_FDm] = detect_beat(Pm, dt);

%% Separación de ciclos ...................................................
tlv = cut_beat(tm,ind_CEm,1);
Plv = cut_beat(Pm,ind_CEm,1);
Vlv = cut_beat(Vm,ind_CEm,1);

j=5;
%% Modelo de corazón.......................................................
Lt = 1;

% Parámetros Modelo Circulacion
Vd = V0; %ml
LVEDV = Vlv(1); %ml

% Elastancia
T = tlv(j,length(tlv(j,:)))-tlv(j,1);
E_vij = Plv(j,:)./(Vlv(j,:)-Vd);   % Calculamos la elastancia para el cerdo
Et = linspace(0,T,length(E_vij));

%Estimacion de parametros
fh = @(X)costo(E_vij, Et, Plv(j,:), Vd, LVEDV, X);

x0=[28.1875, 0.1326];
X = lsqnonlin(fh,x0,[10,0.1],[50,1]);
Pv = X(1);
Rv = X(2);

%% Calculo de Volumen y Presion -------------------------------------------
[t,V] = ode45(@(t,y) Vdot(t,y,Et,E_vij, Rv, Pv, Vd), Et, LVEDV);

% Calculo de Presion ------------------------------------------------------

P = E_vij'.*(V-Vd);

% Grafica -----------------------------------------------------------------
createfigure_PV_LV(Et,Vlv(j,:),t,V,Plv(j,:),P);


%% Windkessel
% Calculo de ES
E_vi = P./(V-V0);
[E_ESj,ind_ESj]=max(E_vi);

ind_CEj = 1;

%% Flujo
Q_in = flujo(V,dt, ind_CEj, ind_ESj);

% Presion aortica sistolica
Pao_s = P(ind_CEj:ind_ESj);
x = length(t)-length(Pao_s);
%Pao_d = linspace(P(ind_ESj), P(ind_CEj), length(t)-length(Pao_s)+1);

t_aux = [t(ind_ESj:length(t))];
% t_aux = [t(j,ind_ESj:length(t(j,:))) (t(j,2:ind_CEj)-t(j,1)+t(j,length(t(j,:))))];
t_aux = t_aux - t_aux(1);
descenso = fit([0; t_aux(length(t_aux)-1:length(t_aux))],[P(ind_ESj); P(ind_CEj)+0.01; P(ind_CEj) ], 'a*exp(b*x)+c','StartPoint',[1,-0.1,P(ind_CEj)]);

Pao_d =descenso.a*exp(descenso.b*t_aux)+descenso.c;

Pao_true = zeros(length(t),2);
Pao_true(:,1)=t;
Pao_true(:,2)=[Pao_s' Pao_d(2:length(Pao_d))'];

% Flujo final
Q_modelo = zeros(length(V),2);
Q_modelo(:,1)=t;
Q_modelo(:,2)=Q_in;


%% Estimación Pao
PAo_CE = P(ind_CEj);

% Parametros WindKessel
    % Rp [mmHg.s/ml]
    % L [mmHg.s2/ml]
    % Ra [mmHg.s/ml]
    % C [ml/mmHg]
% Valores ajustados

% C = 3.842
% L = 7.8126
% Ra = 0.010204
% Rp = 1.0089
% Rw = 0.0053734
    
% C = 4.6232;
% L = 5.0757;
% Ra = 0.010502;
% Rp = 0.96265;
% Rw = 0.0066672;

%% LLamado modelo simulink
mdl = 'WK5';
open_system(mdl);
%in = Simulink.SimulationInput(mdl);
sim(mdl,t);

% QLV = out.QLV;
% Pao = out.Pao;
%% Flujo aórtico
ind = QLV.signals.values>0;
ind(1) = 1;
Qstored = zeros(size(QLV.time));
Qstored(~ind) = QLV.signals.values(~ind) - (Pao.signals.values(~ind)/(Rp));
Qstored(ind) = QLV.signals.values(ind) ;
%Qstored = QLV.signals.values - (Pao2/(Rp));
Ts = QLV.time(2)-QLV.time(1);

AoEDV = 5;
VAo = AoEDV+cumtrapz(Qstored)*Ts;

EAo = Pao.signals.values ./ VAo;

ind = Q_modelo(:,2)>0;
ind(1) = 1;
Qstored(~ind) = Q_modelo(~ind,2) - Pao_true(~ind,2)/(Rp);
Qstored(ind) = Q_modelo(ind,2) ;
%Qstored = QLV.signals.values - (Pao2/(Rp));
Ts = Q_modelo(2,1)-Q_modelo(1,1);

AoEDV = 5;
VAo2 = AoEDV+cumtrapz(Qstored)*Ts;

EAo = Pao_true(:,2) ./ VAo;

%% Visualizacion-----------------------------------------------------------

figure()
plot(t,P)
hold on
plot(t,Pao_true(:,2))
plot(Pao.time, Pao.signals.values)
hold off
legend('P_{LV}', 'P_{ao}','P_{ao_{WK}}');

figure()
plot(QLV.time,QLV.signals.values)
hold on
plot(QLV.time,Qstored)
hold off
legend('Q_{LV}', 'Q_{ao_{WK}}');


% Graficar bucles superpuestos
fig = grafica_bucles(P, V, VAo, Pao.signals.values);
fig = grafica_bucles(P, V, VAo2, Pao_true(:,2));
% P_CE = P(1);
%     V_CE = V(1);
% 
%     Pao_CE =Pao_true(1,2);
%     Vao_CE = VAo(1);
% 
% 
%     fig = figure(300);
%     hold on
%     % Flip bucle aortico
%     plot(V_CE-(VAo-Vao_CE),Pao_true(:,2))
%     plot(V,P)
%     plot(V_CE,P_CE,'o')
%     plot(V_CE,Pao_CE,'o')
%     hold off
%     % plot(V_FS-(VAo-Vao_FS),Pao2)
%     xlabel('Volumen (ml)')
%     ylabel('Presión (mmHg)')
%     title('Bucle PV arterial')
%     legend('Bucle PV arterial','Bucle PV estimado')
    
% figure()
% subplot(221)
% subplot(222)
% subplot(223)
% subplot(224)
