clc
close all

%%
filename = 'NOV08010.csv';
opts = detectImportOptions(strcat(['Datos_cerdo/',filename]));

T = readtable(strcat(['Datos_cerdo/',filename]));

a = split(T.tstamp(1:2),':');
dt = str2double(a{2,length(a)}) - str2double(a{1,length(a)});

%% Defino las variables a usar ............................................
Pm = T.LVP;
Vm = T.LVV;
tm = 0:dt:dt*(length(Pm)-1);

%% Detección de ciclos ....................................................
[ind_CEm, ind_FDm] = detect_beat(Pm, dt);
% ind = Valentinuzzi(Pm, dt, bpFilt);
ind_FDm = ind_FDm-10;

%% Separación de ciclos ...................................................
tLV = cut_beat(tm,ind_FDm,1);
PLV = cut_beat(Pm,ind_FDm,1);
VLV = cut_beat(Vm,ind_FDm,1);

j=5;

%% Analisis de maniobra ...................................................

tol = 0.5;
ind = 1:length(PLV(:,1));
[Emax_r, V0_r, Vfs, Pfs, e] = iteracion(PLV, VLV, tol);
% ind = [1:21, 25:length(PLV(:,1))];
% [Emax_r, V0_r, Vfs, Pfs, e] = iteracion(PLV(ind,:), VLV(ind,:), tol);
% [Emax_r, V0_r, Vfs, Pfs, e] = iteracion(PLV(1:length(PLV(:,1))-12,:), VLV(1:length(PLV(:,1))-12,:), tol);

%% Gráficas ...............................................................
figure()
for i=ind
    hold on
    plot(VLV(i,:),PLV(i,:))
end
plot(Vfs,Pfs,'k*')
V = [V0_r, max(Vfs)+10];
plot(V,Emax_r*(V-V0_r), 'LineWidth',1.5,'Color','b')

% plot(VLV(j,:),PLV(j,:),'LineWidth',3)

% plot(V_ES,P_ES,'o')
% plot(V_ISO,P_ISO,'o')
% plot([V0, V_ISO],E_ES*[V0, V_ISO] + k)
% plot(VLV(j,ind_FD),PLV(j,ind_FD),'o')
hold off