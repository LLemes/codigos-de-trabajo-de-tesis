%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         Prueba de circuito WK
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Lt = 1;
%% Parámetros Modelo Circulacion
global Rv
global Pv
global Vd

Pv = 34.8758; %mmHg
Vd = 222.8843; %ml
Rv = 0.2557;

% Elastancia

global T
global Tp
% global Emax
% global Emin
% 
% Emax = 2.31; %mmHg/ml
% Emin = 0.06; %mmHg/ml

T=1.0;


% =========================================================================
%           Elastancia a partir de Datos de Edwards (chancho)
% =========================================================================

i = 1;
filename = strcat(['Datos PV Edward/Datos PV ',num2str(i),'.xlsx']);
opts = detectImportOptions(filename);
opts.VariableUnitsRange = 'A3';
opts.VariableTypes = {'double', 'double', 'double', 'double', 'double', 'double', 'double', 'double', 'double', 'double'};

Tc = readtable(filename, opts);
dt = Tc.Time(2)-Tc.Time(1);
% Calculo de Elastancia
PLV = Tc.LVPressure;
VLV = Tc.LVTotalVolume;
V0 = 222.8843;

ETf = PLV./(VLV-V0);    % Calculamos la elastancia para el cerdo

% Elastancia
PN = PLV(1351:1585);
VN = VLV(1351:1585); 
EN = PN./(VN-Vd);    % Consideramos un latido como "molde"
% EN = EN*Emax/max(EN);   % Se "ajusta" a los parámetros de humano
% EN = EN-min(EN)+Emin;
[~,ind_p]=max(EN);

E = EN; 
P = PN;
V = VN;                 
for i=1:Lt-1
    E = cat(1,E,EN);    % Se repiten los ciclos
    P = cat(1,P,PN);    % Se repiten los ciclos
    V = cat(1,V,VN);    % Se repiten los ciclos
end

tN = linspace(0,T,length(PN));
Et = linspace(0,Lt*T,length(E));
Tp = Et(ind_p);


dE = gradient(E)./dt;
% Cálculo de presión
% LVEDP = 14; %mmHg
% [t,PLV] = ode45(@(t,y) Pdot(t,y,Et,E,dE), [0 Lt], [LVEDP]);

% E = interp1(Et,E,t);
% dE = interp1(Et,dE,t);
% P = interp1(Et,P,t);
% V = interp1(Et,V,t);


%% Flujo

Q = -gradient(V)/dt;

Q2 = (P - Pv)/Rv;

dP = gradient(P)/dt;
dE = gradient(E)/dt;
Q3 = -(dP - dE.*P./E)./E;
% Q2(Q2<0)=0;

flujo=2;

if flujo==1
    Q_modelo = zeros(length(Q),2);
    Q_modelo(:,1)=Et;
    Q_modelo(:,2)=Q;
else if flujo==2
    Q_modelo = zeros(length(Q2),2);
    Q_modelo(:,1)=Et;
    Q_modelo(:,2)=Q2;
    else
        Q_modelo = zeros(length(Q3),2);
        Q_modelo(:,1)=Et;
        Q_modelo(:,2)=Q3;
    end
end

%%
LVEDV = 304; %ml

[t,VLV2] = ode45(@(t,y) Vdot(t,y,Et,E), [0 Lt], LVEDV);

%...........................................
E = interp1(Et,E,t);
PLV2 = E.*(VLV2-Vd);
QLV2 = -gradient(VLV2)/(t(2)-t(1));
QLV2(QLV2<0)=0;

figure()
subplot(2,1,1)
plot(t,VLV2)
hold on
plot(Et,V)
hold off

subplot(2,1,2)
plot(t,QLV2)
hold on
plot(Et,Q)
plot(t,(PLV2 - Pv)/Rv);

CVP=5;
CO = (max(VLV2)-min(VLV2))/max(t); %ml/s
% MAP = mean(PAo);
% Rtot = (mean(MAP) - CVP)/CO;    %mmHg*s/ml
Rtot = (99.7-CVP)/CO;
Qstored = QLV2 - (PLV2/Rtot);
plot(t,Qstored);
hold off
% plot(t,PLV2)
% hold on
% plot(Et,P)
% hold off

figure()
plot(VLV2,PLV2)
hold on
plot(V,P)


AoEDV = 0;
V = AoEDV+cumtrapz(Qstored);