PAo_CE = P(ind_CEj);

% Parametros WindKessel
    % Rp [mmHg.s/ml]
    % L [mmHg.s2/ml]
    % Rw [mmHg.s/ml]
    % C [ml/mmHg]

if inum==1    
%     C = 2.4755;
%     L = 0.00568;
%     Rp = 1.6138;
%     Rw = 0.048873;
    
    C = 2.3174;
    L = 0.00023991;
    PAo_CE = 63.766;
    Rp = 1.6972;
    Rw = 0.052201;
    
%     Rp=0.5*Rp;
%     Rp=2*Rp;
%     C=0.5*C;
%     C=2*C;
%     L=0.5*L;
%     L=2*L;
%     Rw=0.5*Rw;
%     Rw=2*Rw;
%     Rw=0;
% elseif inum==2
%     C = 3.1659;
%     L = 0.012443;
%     Rp = 1;
%     Rw = 1.3984e-07;
% elseif inum==3
%     C = 2.5655;
%     L = 0.012661;
%     Rp = 1.0964;
%     Rw = 1.9814e-08;
end

%% LLamado modelo simulink
mdl = 'IVWK';
open_system(mdl);
sim(mdl,Q_modelo(:,1));

%% Flujo aórtico
ind = QLV.signals.values>0;
ind(1) = 1;

Q_perdido = -(Pao.signals.values/(Rp));
A = trapz(Pao.time, Q_perdido)/trapz(Pao.time(~ind), Q_perdido(~ind));

Qstored(~ind) = QLV.signals.values(~ind) - A*(Pao.signals.values(~ind)/(Rp));
Qstored(ind) = QLV.signals.values(ind);
%Qstored = QLV.signals.values - (Pao2/(Rp));
Ts = QLV.time(2)-QLV.time(1);

%%
AoEDV = 5;
VAo = AoEDV+cumtrapz(Qstored)*Ts;

EAo = Pao.signals.values ./ VAo';


VAo2 = AoEDV+cumtrapz(QLV.signals.values-(Pao.signals.values/Rp))*Ts;

EAo2 = Pao.signals.values ./ VAo2;
%% Visualizacion-----------------------------------------------------------

figure()
plot(t,P)
hold on
plot(Pao_true(:,1),Pao_true(:,2))
plot(Pao.time, Pao.signals.values)
hold off
legend('P_{LV}', 'P_{ao}','P_{ao_{WK}}');

figure()
plot(QLV.time,QLV.signals.values)
hold on
plot(QLV.time,Qstored)
hold off
legend('Q_{LV}', 'Q_{ao_{WK}}');


% Graficar bucles superpuestos
% fig = grafica_bucles(P, V, VAo, Pao);


% figure()
% subplot(221)
% subplot(222)
% subplot(223)
% subplot(224)

%% Visualizacion-------------------------------------------------------------

figure()
subplot(121)
plot(t,P)
hold on
plot(Pao_true(:,1),Pao_true(:,2))
plot(Pao.time, Pao.signals.values)
hold off
legend('P_{LV}', 'P_{Ao} reconstruida','P_{Ao_{WK}}');

subplot(122)
plot(QLV.time,QLV.signals.values)
hold on
plot(QLV.time,Qstored)
plot(QLV.time,QLV.signals.values-(Pao.signals.values/Rp))
hold off
legend('Flujo de entrada al WK','Flujo arterial (MQM)', 'Flujo arterial (MQ)');



figure()

subplot(321)
plot(QLV.time,QLV.signals.values)
hold on 
plot(QLV.time,Qstored)
plot(QLV.time,QLV.signals.values-(Pao.signals.values/Rp))
hold on
title('Flujo')
ylabel('Flujo (ml.s^{-1})')
xlabel('Tiempo (s)')
legend('Flujo de entrada al WK','Flujo arterial (MQM)', 'Flujo arterial (MQ)');

subplot(323)
% plot(t,P)
hold on
plot(Pao.time, Pao.signals.values)
plot(Pao_true(:,1),Pao_true(:,2))
axis tight
hold off
title('Presión')
ylabel('Presión (mmHg)')
xlabel('Tiempo (s)')
% legend('P_{LV}', 'P_{Ao} estimada por WK', 'P_{Ao} reconstruida');
legend('P_{Ao} estimada por WK', 'P_{Ao} reconstruida');

subplot(325)
E2 = interp1(t,E_vi,Pao.time);
P2 = interp1(t,P,Pao.time);
[~,ind_ES2]=max(E2);
dP = gradient(P2(:))/dt;
% pico negativo de d2P/dt2 
d2P = gradient(dP)/dt;
[~, ind_CE] = min(d2P(1:ind_ES2-5));

P_CE = P2(ind_CEj);
V_CE = V(ind_CEj);

Pao_CE = Pao.signals.values(ind_CEj);
Vao_CE = VAo(ind_CEj);
Vao2_CE = VAo2(ind_CEj);

plot(Pao.time,V_CE-(VAo-Vao_CE))
hold on
plot(Pao.time,V_CE-(VAo2-Vao2_CE))
hold off
axis tight
title('Volumen Arterial')
ylabel('Volumen (ml)')
xlabel('Tiempo (s)')
legend('Volumen Arterial (MQM)', 'Volumen Arterial (MQ)');

subplot(122)
hold on
plot(V,P)
% Flip bucle aortico
plot(V_CE-(VAo-Vao_CE),Pao.signals.values)
plot(V_CE-(VAo2-Vao2_CE),Pao.signals.values)

% plot(V_FS-(VAo-Vao_FS),Pao2)
xlabel('Volumen (ml)')
ylabel('Presión (mmHg)')
title('Bucle PV')
legend('Ventricular','Arterial estimado (MQM)','Arterial estimado (MQ)')


%% Guardado de datos
% filename = 'oveja_sa';
% 
% T3 = table();
% T3.t=Pao.time;
% T3.Pao=Pao.signals.values;
% T3.Qin=QLV.signals.values;
% T3.Vao=VAo;
% T3.EAo=EAo;
% head(T3)
% T3.Rp=ones(length(VAo),1)*Rp;
% T3.C=ones(length(VAo),1)*C;
% T3.Ra=ones(length(VAo),1)*Ra;
% T3.L=ones(length(VAo),1)*L;
% T3.Rw=ones(length(VAo),1)*Rw;
% head(T3)
% writetable(T3,filename)


