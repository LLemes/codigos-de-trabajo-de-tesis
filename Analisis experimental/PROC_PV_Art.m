%==========================================================================
%Prueba determinaci�n del bucle presi�n volumen A�RTICO con se�aes del
%paper QUICK et al. (PERRO 2)
%
%Se considera un WK de modo que el volumen arterial se calcula a partir del
%flujo de almacenamiento (capacitivo), que es la diferencia entre el flujo
%inyectado por el ventr�culo (Q_in) y el flijo de salida (Q_out=P_Ao/RP)
%
%                           VA=int(Q_in-P_Ao/Rp)
%
%==========================================================================
%Carga de Datos
PV=load('Serie 2_P_Q.txt');
P_Ao=PV(:,1);
Q_in=PV(:,2);
%Filtrado de las señales
N=5;
P_Ao=filtfilt(ones(1,N)/N,1,P_Ao);
Q_in=filtfilt(ones(1,N)/N,1,Q_in);

t=linspace(0,0.45,length(P_Ao));
Ts=t(2)-t(1);
Lt=max(t);
T=1;

%Flujo Windkessel
Q_modelo = zeros(length(t),2);
Q_modelo(:,1)=t;
Q_modelo(:,2)=Q_in;

%Presion Windkessel
PAo_CE = P_Ao(1);

%Parametros Windkessel
Rp = 2.775; %mmHg.s/ml
C = 0.5826;
Ra = 0.1;
L = 0.07575;

Rw=0;



% Num = [(C*Ra*(Rw+Rp)+C*Rw) (Ra+Rp)];
% Den = [(C*(Rw+Rp)) (1)];

Num = [(Rw*C*L*Rp + Ra*Rw*L*C + Ra*Rp*L*C) (Ra*L + Ra*Rp*Rw*C + Rp*L) (Ra*Rp)];
Den = [(L*C*(Rp+Rw)), (Ra*(Rp+Rw)*C + L), (Ra)];


%Para setear condiciones inciales
[Ax,Bx,Cx,Dx] = tf2ss(Num,Den);

WK4p()

%% DeterminaciOn del volumen almacenado Q_Almac
%VOLUMEN ARTERIAL ALMACENADO
V_AA=cumsum(QLV.signals.values-( Pao.signals.values/Rp))*Ts;
V_AA=cumtrapz(QLV.signals.values-( Pao.signals.values/Rp))*Ts;

%ASIGNACI{ON DE UN VALOR DE VOLUMEN MEDIO
VA_Medio=2;
V_AA=V_AA+VA_Medio;

%ELASTANCIA AORTICA
EAO= Pao.signals.values./V_AA;

% %VOLUMEN ARTERIAL ALMACENADO
% V_AA=cumsum(Q_in-(P_Ao/Rp))*Ts;
% V_AA=cumtrapz(Q_in-(P_Ao/Rp))*Ts;
% 
% %ASIGNACI{ON DE UN VALOR DE VOLUMEN MEDIO
% VA_Medio=2;
% V_AA=V_AA+VA_Medio;
% 
% %ELASTANCIA AORTICA
% EAO=P_Ao./V_AA;


%% Visualizacion-------------------------------------------------------------
figure()
plot(t,P_Ao)
hold on
plot(Pao.time, Pao.signals.values)
hold off
legend('P_{ao}','P_{ao_{WK}}');

figure()
subplot(221),plot(Pao.time, Pao.signals.values); axis tight; 
legend('P Aortica');
subplot(223),plotyy(QLV.time, QLV.signals.values,QLV.time,V_AA); hold on; plot(t,t*0,':k'); plot(QLV.time,(QLV.signals.values-( Pao.signals.values/Rp))); axis tight;
legend('Flujo Arterial','Qstored','Volumen Arterial');
subplot(222),plot(V_AA, Pao.signals.values); axis tight;
legend('Bucle Presion Volumen Arterial');
subplot(224),plot(Pao.time,EAO); axis tight;
legend('Elastancia Arterial');


% figure()
% subplot(221),plot(t,P_Ao); axis tight; 
% legend('P Aortica');
% subplot(223),plotyy(t,Q_in,t,V_AA); hold on; plot(t,t*0,':k'); plot(t,Q_in-(P_Ao/Rp)); axis tight;
% legend('Flujo Arterial','Qstored','Volumen Arterial');
% subplot(222),plot(V_AA,P_Ao); axis tight;
% legend('Bucle Presion Volumen Arterial');
% subplot(224),plot(t,EAO); axis tight;
% legend('Elastancia Arterial');

