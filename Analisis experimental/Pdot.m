function dPdt = Pdot(t,P,Et,E,dE)
    global Rv
    global Pv

    % Elastancia mediante modelado
    E = interp1(Et,E,t); % Interpolate the data set (gt,g) at time t
    dEdt = interp1(Et,dE,t);
    
    % Derivada de Presion
%     dPdt = dEdt*P/E + E*(Pv-P)/(Rv);
    dPdt = dEdt*P/E + E*(Pv-P)/(Rv);
end