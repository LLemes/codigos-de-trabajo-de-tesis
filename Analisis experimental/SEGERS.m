clc
clear all
close all

VoP = 1;        % 0: se calcula primero el volumen. 
Elastancia = 1; % 0: E de cerdo.

%% Parámetros Modelo Circulacion
global Rv
global Pv
global Vd

Pv = 7.50; %mmHg
Vd = 20.0; %ml
Rv = 0.03;

% Elastancia

global T
global Tp
global a1
global a2
global n1
global n2
global Emax
global Emin

a1 = 0.303;
a2 = 0.508;
n1 = 1.32;
n2 = 21.9;
Emax = 2.31; %mmHg/ml
Emin = 0.06; %mmHg/ml

T=1.0;

% Tiempo
Lt = 4;

if Elastancia == 0
    % =====================================================================
    %           Elastancia a partir de Datos de Edwards (chancho)
    % =====================================================================

    i = 1;
    filename = strcat(['Datos PV Edward/Datos PV ',num2str(i),'.xlsx']);
    opts = detectImportOptions(filename);
    opts.VariableUnitsRange = 'A3';
    opts.VariableTypes = {'double', 'double', 'double', 'double', 'double', 'double', 'double', 'double', 'double', 'double'};

    Tf = readtable(filename, opts);
    dt = Tf.Time(2)-Tf.Time(1);
    % Calculo de Elastancia
    PLV = Tf.LVPressure;
    VLV = Tf.LVTotalVolume;
    V0 = 222.8843;

    ETf = PLV./(VLV-V0);    % Calculamos la elastancia para el cerdo

    % Elastancia
    EN = ETf(1351:1585);    % Consideramos un latido como "molde"
    %EN = EN*Emax/max(EN);   % Se "ajusta" a los parámetros de humano
    %EN = EN-min(EN)+Emin;
    [~,ind_p]=max(EN);

    E = EN;                 
    for i=1:Lt-1
        E = cat(1,E,EN);    % Se repiten los ciclos
    end

    Et = linspace(0,Lt*T,length(E));
    Tp = Et(ind_p);

    if VoP == 0
        % Cálculo de Volumen
        LVEDV = 60; %ml

        [t,VLV] = ode45(@(t,y) Vdot(t,y,Et,E), [0 Lt], LVEDV);

        %...........................................
        E = interp1(Et,E,t);
        PLV = E.*(VLV-Vd);
    else
        dE = gradient(E)/dt;
        % Cálculo de presión
        LVEDP = 14; %mmHg
        [t,PLV] = ode45(@(t,y) Pdot(t,y,Et,E,dE), [0 Lt], [LVEDP]);

        %...........................................
        E = interp1(Et,E,t);
        VLV = (PLV./E)+Vd;
    end
    
else
    % =====================================================================
    %                   Elastancia mediante modelado
    % =====================================================================
    
    dt=0.01;
    Et = 0:dt:Lt*T;
    
    tc = Et-(floor(Et/T)*T); % Es  periódica, con periodo T

    k1 = (tc/(a1*T)).^n1;
    H1 = k1./(1+k1);

    k2 = (tc/(a2*T)).^n2;
    H2 = 1./(1+k2);

    E = (H1.*H2);
    E = E/max(E) * Emax;
    E = E-min(E)+Emin;

    [~,ind_p]=max(E);
    Tp = Et(ind_p);

    if VoP == 0
        % Cálculo de Volumen
        LVEDV = 95; %ml

        [t,VLV] = ode45(@(t,y) Vdot(t,y,Et,E), [0 Lt], LVEDV);

        %...........................................
        E = interp1(Et,E,t);
        PLV = E.*(VLV-Vd);
    else
        dE = gradient(E)/dt;
        % Cálculo de presión
        LVEDP = 5; %ml
        [t,PLV] = ode45(@(t,y) Pdot(t,y,Et,E,dE), [0 Lt], [LVEDP]);

        %...........................................
        E = interp1(Et,E,t);
        VLV = (PLV./E)+Vd;
    end

end

%% Calculo de Flujo

dt=t(2)-t(1);
Q2 = -(Pv-PLV)/Rv;
% Q2(Q2<0)=0;

Q_modelo = zeros(length(t),2);
Q_modelo(:,1)=t;
Q_modelo(:,2)=Q2;

%% Gráficas

figure()
plot(t,PLV)
title('Presión ventricular')
xlabel('Tiempo')
ylabel('Presión (mmHg)')

figure()
plot(t,Q2)
title('Flujo')
xlabel('Tiempo')
ylabel('Caudal (ml/s)')

figure()
plot(t,VLV)
title('Volumen ventricular')
xlabel('Tiempo')
ylabel('Volumen (ml)')

figure()
plot(t,E)
title('Elastancia ventricular')
xlabel('Tiempo')
ylabel('Elastancia (mmHg/ml)')

figure()
plot(VLV,PLV)
hold on
[~,ind_p]=findpeaks(E,'MinPeakHeight',1);
plot(VLV(ind_p),PLV(ind_p),'o')

v=[20 60];
plot(v,Emax*(v-Vd))
hold off
title('Bucle PV')
xlabel('Volumen (ml)')
ylabel('Presión (mmHg)')
legend('Bucle PV', 'Punto E_{FS}', ['Recta Emax\n Emax=%f \n V_0=%f', Emax, Vd]);