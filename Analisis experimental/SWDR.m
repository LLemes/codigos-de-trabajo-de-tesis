%% Calculo de damping


%Metodo deportistas

	% Trabajo
    aux_s = trapz(V(ind_CE:ind_ES2), P2(ind_CE:ind_ES2));
    aux_d = trapz(V([ind_ES2:length(V), 1:ind_CE]), P2([ind_ES2:length(V), 1:ind_CE]));
    W = abs(aux_s) - abs(aux_d);
    
    % Jopo del bucle    
    aux_p = linspace(P2(ind_ES2),P2(ind_CE),length(P2([ind_ES2:length(V), 1:ind_CE])));
    aux_d2 = trapz(V([ind_ES2:length(V), 1:ind_CE]),aux_p);
    Wdis = abs(aux_s) - abs(aux_d2);
    Wel = W-Wdis;
    
    damping = Wdis/Wel;
    

%Bucles estimados
    % Trabajo arterial
    aux_sart = trapz(VAo(ind_CE:ind_ES2), Pao.signals.values(ind_CE:ind_ES2));
    aux_dart = trapz(VAo([ind_ES2:length(V), 1:ind_CE]), Pao.signals.values([ind_ES2:length(V), 1:ind_CE]));
    Wart = abs(aux_sart) - abs(aux_dart);
    Wel_a = W-Wart;
    
    damping_a = Wart/Wel_a;
    damping_t = Wart/W;
