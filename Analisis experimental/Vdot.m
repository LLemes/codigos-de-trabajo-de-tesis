function [dVdt] = Vdot(t,V,Et,E,Rv, Pv, Vd)

    % Elastancia mediante modelado
    E = interp1(Et,E,t); % Interpolate the data set (gt,g) at time t
    
    % Derivada del Volumen
    dVdt = (Pv - (E.*(V-Vd)))/Rv;

end