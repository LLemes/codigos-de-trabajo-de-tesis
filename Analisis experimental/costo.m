function c = costo(E_vij, Et, Plv, Vd, LVEDV, X)
    Pv = X(1);
    Rv = X(2);
    [~,V] = ode45(@(t,y) Vdot(t,y,Et,E_vij, Rv, Pv, Vd), Et, LVEDV);

    % Calculo de Presion ------------------------------------------------------

    P = E_vij'.*(V-Vd);
    
    Vlv = P./E_vij +Vd;
    c = (P'-Plv);
end


