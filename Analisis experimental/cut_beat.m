function Scut=cut_beat(S,ind,FLAG)
    Scut=[];
    max_ind = max(diff(ind));
    if FLAG
        lim = 1:(length(ind)-1);
    else
        lim = 1:(length(ind)-6);
    end
    for i=lim
        Sc = S(ind(i):ind(i+1));
        
        x = linspace(0,1,length(Sc));
        x_new = linspace(0,1,max_ind);
        Sr = interp1(x,Sc,x_new,'spline'); %vector índice X del largo de tu ciclo, un ciclo de la señal, vector índice X2 del largo que querés
        
        Scut = [Scut ; Sr];

    end
end