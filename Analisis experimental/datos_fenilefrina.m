% Datos con fenilefrina

file1 = 'Fenilefrina.csv';
file2 = 'Normal.csv';

Tc1 = readtable(file1, 'Delimiter',';');

Tc1.P = replace(Tc1.P,',','.');
Tc1.P = str2double(Tc1.P);

Tc1.V = replace(Tc1.V,',','.');
Tc1.V = str2double(Tc1.V);

[~,p]=max(gradient(Tc1.P));
p=p-10;
aux = Tc1.P(1:p);
P(1:length(Tc1.P)-p)=Tc1.P(p+1:length(Tc1.P));
P(length(Tc1.P)-p+1:length(Tc1.P))=aux;

aux = Tc1.V(1:p);
V(1:length(Tc1.P)-p)=Tc1.V(p+1:length(Tc1.P));
V(length(Tc1.P)-p+1:length(Tc1.P))=aux;

t = linspace(0,1,length(Tc1.P))';
V0=0;

%% Windkessel -------------------------------------------------------------
% Calculo de ES
E_vi = P./(V-V0);
[E_ESj,ind_ESj]=max(E_vi);
[~, ind_ESj]=max(V);
ind_CEj = 15;

%% Flujo
Q_in = flujo(V,t(2)-t(1), ind_CEj, ind_ESj);

% Presion aortica sistolica
Pao_s = P(ind_CEj:ind_ESj);
x = length(t)-length(Pao_s);
t_aux = t(ind_ESj:length(t));
t_aux = t_aux - t_aux(1);
descenso = fit([0; t_aux(length(t_aux)-1:length(t_aux))],[P(ind_ESj); P(ind_CEj)+0.01; P(ind_CEj) ], 'a*exp(b*x)','StartPoint',[1,-0.1]);

Pao_d =descenso.a*exp(descenso.b*t_aux);

Pao_true = zeros(length(Pao_s)+length(Pao_d)-1,2);
Pao_true(:,1)=t;
Pao_true(:,2)=[Pao_s; Pao_d(2:length(Pao_d))];

% 
% % Presion aortica sistolica
% Pao_s = P(ind_CEj:ind_ESj);
% x = length(t)-length(Pao_s);
% Pao_d = linspace(P(ind_ESj), P(ind_CEj), length(t)-length(Pao_s)+1);
% 
% Pao_true = zeros(length(t),2);
% Pao_true(:,1)=t;
% Pao_true(:,2)=[Pao_s' Pao_d(2:length(Pao_d))];

% Flujo final
Q_modelo = zeros(length(V),2);
Q_modelo(:,1)=t;
Q_modelo(:,2)=Q_in;

%% Presion aortica

% [~,ind_ES] = max(E);
% dP = gradient(P(:))/dt;
% 
% % % como el 10% previo al máximo de la derivada de presión
% % aux = cumsum(dPLV(1:ind_M)>(M*0.9));
% % [~, ind_CE] = max(aux == 1);
% 
% % pico negativo de d2P/dt2 
% d2P = gradient(dP)/dt;
% [~, ind_CE] = min(d2P(1:ind_ES-5));

P_CE = P(ind_CEj);
V_CE = V(ind_CEj);

PAo = P(ind_CEj+1:ind_ESj);
PAo_t = t(ind_CEj+1:ind_ESj);

%% Flujo "verdadero" final

Q_modelo2 = zeros(length(PAo_t),2);
Q_modelo2(:,1)=PAo_t;
Q_modelo2(:,2)=Q_in(ind_CEj+1:ind_ESj);

%     