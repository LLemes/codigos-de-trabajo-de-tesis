clc
clear all
close all

%% Lt = 1;

% =========================================================================
%           Elastancia a partir de Datos de Edwards (chancho)
% =========================================================================

inum = 1;
filename = strcat(['Datos PV Edward/Datos PV ',num2str(inum),'.xlsx']);
opts = detectImportOptions(filename);
opts.VariableUnitsRange = 'A3';
opts.VariableTypes = {'double', 'double', 'double', 'double', 'double', 'double', 'double', 'double', 'double', 'double'};

Tc = readtable(filename, opts);
dt = Tc.Time(2)-Tc.Time(1);

%% Estimacion de Parterial
% Defino las variables a usar .............................................
Pm = Tc.LVPressure;
Vm = Tc.LVTotalVolume;
tm = 0:dt:dt*(length(Pm)-1);

% Detección de ciclos .....................................................
[ind_CEm, ind_FDm] = detect_beat(Pm, dt);


%% Separación de ciclos ...................................................
tlv = cut_beat(tm,ind_FDm-10,1);
Plv = cut_beat(Pm,ind_FDm-10,1);
Vlv = cut_beat(Vm,ind_FDm-10,1);

j=5;

%% Analisis de latido unico ...............................................
[ind_FD, ind_ES, E_ES, V0, P_ISO] = latido_unico(tlv(j,:), Plv(j,:), Vlv(j,:));


%% Separación de ciclos ...................................................
tlv = cut_beat(tm,ind_CEm,1);
Plv = cut_beat(Pm,ind_CEm,1);
Vlv = cut_beat(Vm,ind_CEm,1);

%% Modelo de corazón.......................................................
Lt = 1;

% Parámetros Modelo Circulacion
Vi = Vlv(1); %ml

% Elastancia
T = tlv(j,length(tlv(j,:)))-tlv(j,1);
E_vij = Plv(j,:)./(Vlv(j,:)-V0);   % Calculamos la elastancia para el cerdo
Et = linspace(0,T,length(E_vij));

%Estimacion de parametros
fh = @(X)costo(E_vij, Et, Plv(j,:), V0, Vi, X);

x0=[28.1875, 0.1326];
X = lsqnonlin(fh,x0,[10,0.1],[50,1]);
Pv = X(1);
Rv = X(2);

%% Calculo de Volumen y Presion -------------------------------------------

[t,V] = ode45(@(t,y) Vdot(t,y,Et,E_vij, Rv, Pv, V0), Et, Vi);

% Calculo de Presion ------------------------------------------------------

P = E_vij'.*(V-V0);

% Grafica -----------------------------------------------------------------
createfigure_PV_LV(Et,Vlv(j,:),t,V,Plv(j,:),P);

%% Windkessel -------------------------------------------------------------
% Calculo de ES
E_vi = P./(V-V0);
[E_ESj,ind_ESj]=max(E_vi);

ind_CEj = 1;

%% Flujo
Q_in = flujo(V,dt, ind_CEj, ind_ESj);

% Presion aortica sistolica
Pao_s = P(ind_CEj:ind_ESj);
x = length(t)-length(Pao_s);
t_aux = t(ind_ESj:length(t));
t_aux = t_aux - t_aux(1);
descenso = fit([0; t_aux(length(t_aux)-1:length(t_aux))],[P(ind_ESj); P(ind_CEj)+0.01; P(ind_CEj) ], 'a*exp(b*x)','StartPoint',[1,-0.1]);

Pao_d =descenso.a*exp(descenso.b*t_aux);

Pao_true = zeros(length(Pao_s)+length(Pao_d)-1,2);
Pao_true(:,1)=t;
Pao_true(:,2)=[Pao_s; Pao_d(2:length(Pao_d))];

% 
% % Presion aortica sistolica
% Pao_s = P(ind_CEj:ind_ESj);
% x = length(t)-length(Pao_s);
% Pao_d = linspace(P(ind_ESj), P(ind_CEj), length(t)-length(Pao_s)+1);
% 
% Pao_true = zeros(length(t),2);
% Pao_true(:,1)=t;
% Pao_true(:,2)=[Pao_s' Pao_d(2:length(Pao_d))];

% Flujo final
Q_modelo = zeros(length(V),2);
Q_modelo(:,1)=t;
Q_modelo(:,2)=Q_in;

%% Presion aortica

% [~,ind_ES] = max(E);
% dP = gradient(P(:))/dt;
% 
% % % como el 10% previo al máximo de la derivada de presión
% % aux = cumsum(dPLV(1:ind_M)>(M*0.9));
% % [~, ind_CE] = max(aux == 1);
% 
% % pico negativo de d2P/dt2 
% d2P = gradient(dP)/dt;
% [~, ind_CE] = min(d2P(1:ind_ES-5));

P_CE = P(ind_CEj);
V_CE = V(ind_CEj);

PAo = P(ind_CEj+1:ind_ESj);
PAo_t = t(ind_CEj+1:ind_ESj);

%% Flujo "verdadero" final

Q_modelo2 = zeros(length(PAo_t),2);
Q_modelo2(:,1)=PAo_t;
Q_modelo2(:,2)=Q_in(ind_CEj+1:ind_ESj);

    