%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         Prueba de circuito WK
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Lt = 1;
%% Parámetros Modelo Circulacion
global Rv
global Pv
global Vd

Vd = 46.6281; %ml

Rv = 0.5444;
Pv = 39.8050;

% Elastancia

global T
global Tp
% global Emax
% global Emin
% 
% Emax = 2.31; %mmHg/ml
% Emin = 0.06; %mmHg/ml

%T=1.0;


%%
% =========================================================================
%                    Flujo a partir de Datos de oveja
% =========================================================================
i=2;

filename = strcat(['Datos_oveja/Datos_PV_Armentano_Corr_',num2str(i),'.txt']);

opts = detectImportOptions(filename);
To = readtable(filename, opts);   %Datos Ventriculares

dt = 1/250;

% Filtrado
L=10; 
b = (1/L)*ones(1,L);
a = 1;

To.PVIc = filter(b,a,To.Var1);
To.PAoc = filter(b,a,To.Var3);
% To.PVIc = T.PVIc-min(To.PVIc);
To.VVIc = filter(b,a,To.Var2);

% Elastancia
PAoN = To.PAoc(128:270);
PN = To.PVIc(128:270);
VN = To.VVIc(128:270); 
EN = PN./(VN-Vd);     % Consideramos un latido como "molde"
% EN = EN*Emax/max(EN);   % Se "ajusta" a los parámetros de humano
% EN = EN-min(EN)+Emin;

E = EN; 
PLV = PN;
VLV = VN;
PAo = PAoN; 
for i=1:Lt-1
    E = cat(1,E,EN);    % Se repiten los ciclos
    PLV = cat(1,PLV,PN);    % Se repiten los ciclos
    VLV = cat(1,VLV,VN);    % Se repiten los ciclos
    PAo = cat(1,PAo,PAoN);    % Se repiten los ciclos
end


% Ajuste Tiempo
T = (length(EN)-1)*dt;
tN = linspace(0,T,length(PN));
Et = linspace(0,Lt*T,length(E));
PAo_t = Et;

dE = gradient(E)/dt;

%%
% LVEDP = PLV(1); %ml
% 
% [t,P] = ode45(@(t,y) Pdot(t,y,Et,E,dE), [0 Lt*T], LVEDP);
% 
% %...........................................
% E = interp1(Et,E,t);
% [~,ind_p]=max(E);
% Tp = Et(ind_p);
% V = (P./E) + Vd;

LVEDV = 85; %ml

[t,V] = ode45(@(t,y) Vdot(t,y,Et,E), [0 Lt*T], LVEDV);

%...........................................
E = interp1(Et,E,t);
[~,ind_p]=max(E);
Tp = Et(ind_p);
P = E.*(V-Vd);

%%

dt=(t(2)-t(1));
Q = -gradient(V)/dt;

Q2 = (P - Pv)/Rv;

dP = gradient(P)/dt;
dE = gradient(E)/dt;
Q3 = -(dP - dE.*P./E)./E;

flujo=1;

if flujo==1
    Q_in = Q;
else
    if flujo==2
        Q_in = Q2;
    else
        Q_in = Q3;
    end
end

Q_in(Q_in<0)=0;

N=2;
Q_in = filtfilt(ones(1,N)/N,1,Q_in);

Q_in = (Q_in*max(Q))/max(Q_in);


Q_modelo = zeros(length(t),2);
Q_modelo(:,1)=t;
Q_modelo(:,2)=Q_in;

%% Graficas

createfigure_PV_LV(Et,VLV,t,V,PLV,P);
