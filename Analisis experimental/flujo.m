function Q_in = flujo(V,dt, ind_CE, ind_ES)
% Calculo de Flujo

    Q = -gradient(V)/dt;
    Q_in = zeros(1,length(Q));
    Q_in(ind_CE:ind_ES) = Q(ind_CE:ind_ES);
    Q_in(ind_CE) = 0;
    t = ones(1,length(Q))*dt;
    t = cumsum(t)-dt;
    
    figure()
    Qaux = Q;
    Qaux(Qaux<0)=0;
    plot(t, Qaux)
    hold on
    
    N=5;
    Q_in = filtfilt(ones(1,N)/N,1,Q_in);

    Q_in = (Q_in*max(Q))/max(Q_in);

    plot(t,Q_in)
    hold off
end