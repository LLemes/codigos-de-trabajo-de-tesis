function ind_W = Valentinuzzi(S,dt, bpFilt)
    fs=1/dt;
    min_fc = 20/60; %Hz
    
   
    %----------------------------------------------------------------------
    %Derivar dos veces S
    dS = gradient(S)/(1/fs);
%     dS = diff(S)/(1/fs);
    d2S = gradient(dS)/(1/fs);
    
    %Binarizar dS
    bdS = zeros(size(dS));
    bdS(dS>0)=1;
    
    %Rectificar d2S
    rd2S = d2S;
    rd2S(d2S<0)=0;
    
    %Calcular W1
    W1 = bdS.*rd2S;
    
    %Calcular W2
    W2 = zeros(size(W1));
    
    [pks,ind] = findpeaks(W1);
    aux_tol = mean(pks);
    
    Tol_W2 = 0.75*aux_tol; %Oveja
%     Tol_W2 = 0.5*max(W1); %Cerdo
    [pks,ind] = findpeaks(W1,'MinPeakHeight', Tol_W2);
    W2(ind) = pks;
    
    %Calcular W3
    Tol_W3 = 0.3;
    W3 = zeros(size(W1));
    
    Sfilt = filter(bpFilt, S(1:ceil(min_fc*4/dt)));
    [p,f]=periodogram(Sfilt,rectwin(length(Sfilt)),length(Sfilt),fs);
    [~,indfc]=max(p);
    fc = f(indfc);
    
    [pks,ind] = findpeaks(W2,'MinPeakDistance', Tol_W3*(fs/fc));
    W3(ind) = pks;

    retraso = mean(diff(ind))/10;
    ind_W = ind - floor(retraso);
    
    figure()
    subplot(4,1,1)
    plot(S,'LineWidth', 1.5)
    hold on
    plot(ind,S(ind),'o','MarkerFaceColor','r')
    hold off
    ylabel('Presión (mmHg)')
    subplot(4,1,2)
    plot(W1,'LineWidth', 1.5)
    ylabel('W1')
    ylim([-0.5 1.5])
    subplot(4,1,3)
    plot(W2,'LineWidth', 1.5)
    ylabel('W2')
    subplot(4,1,4)
    plot(W3,'LineWidth', 1.5)
    ylabel('W3')
    
end