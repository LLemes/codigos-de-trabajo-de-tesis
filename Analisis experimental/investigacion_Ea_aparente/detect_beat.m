function [indM, ind] = detect_beat(S,dt)
    dis = (1/dt)/(250/60);
    dS = diff(S);
    [pk,indM] = findpeaks(dS, 'MinPeakHeight',0.2*max(dS)+mean(dS),'MinPeakDistance', dis);
    
    %umbral
    u = mean(pk)/4;
    ind = [];
    for i=2:length(dS)
        if (dS(i)>=u)&&(dS(i-1)<u)
            ind = [ind i];
        end
    end
    ind = ind(2:length(ind));
    
    figure()
    subplot(3,1,1)
    plot(S,'LineWidth', 1.5)
    hold on
    plot(indM,S(indM),'o','MarkerFaceColor','r')
    hold off
    ylabel('Presión (mmHg)')
    subplot(3,1,2)
    plot(dS,'LineWidth', 1.5)
    hold on
    plot(indM,dS(indM),'o','MarkerFaceColor','r')
    hold off
    ylabel('derivada de la Presión')
    subplot(3,1,3)
    plot(dS,'LineWidth', 1.5)
    hold on
    plot(ind,dS(ind),'o','MarkerFaceColor','r')
    hold off
    ylabel('puntos de FD')
    
end