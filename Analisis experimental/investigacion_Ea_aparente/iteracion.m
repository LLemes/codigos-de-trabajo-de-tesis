function [Emax,V0,Vm,Pm,e] = iteracion(P,V,tol)
    E=1;
    [Emax,V0, Vm,Pm, e] = ajuste(P, V, 0);
    cont = 1;
    
%     E = Emax
%     V0
%     [Emax,V0, Vm,Pm, e] = ajuste(P, V, V0);
%     cont = cont+1;

    while abs(E-Emax)>tol
        E = Emax
        V0
        [Emax,V0, Vm,Pm, e] = ajuste(P, V, V0);
        cont = cont+1;
        Emax
        V0
        E-Emax
    end
 
end