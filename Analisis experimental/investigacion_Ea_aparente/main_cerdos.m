clc
close all
%%
i = 1;
   
%% Datos de Edwards (chancho)
filename = strcat(['Datos PV Edward/Datos PV ',num2str(i),'.xlsx']);
opts = detectImportOptions(filename);
opts.VariableUnitsRange = 'A3';
opts.VariableTypes = {'double', 'double', 'double', 'double', 'double', 'double', 'double', 'double', 'double', 'double'};

T = readtable(filename, opts);
dt = T.Time(2)-T.Time(1);

%% Hallar filtro de fc
fs=1/dt;
min_fc = 20/60; %Hz
bpFilt = designfilt('bandpassfir', 'StopbandFrequency1', 0.25, 'PassbandFrequency1', 0.8, 'PassbandFrequency2', 2.8, 'StopbandFrequency2', 3.25, 'StopbandAttenuation1', 60, 'PassbandRipple', 1, 'StopbandAttenuation2', 60, 'SampleRate', fs);
 
%% Separación de ciclos
%Detección de ciclos
% ind = detect_beat(T.LVPressure, dt);
ind = Valentinuzzi(T.LVPressure, dt, bpFilt);

%Separación de ciclos
t = cut_beat(T.Time, ind,1)./1000;
PLV = cut_beat(T.LVPressure,ind,1);
VLV = cut_beat(T.LVTotalVolume,ind,1);

j=5;

%% Encontramos el punto de fin de diástole

dPLV = gradient(PLV(j,:));
medio = mean(dPLV);
[M, ind_M]=max(dPLV);
umbral = medio + 0.1*(M-medio);
aux_FD = cumsum(dPLV(1:ind_M) > umbral);

ind_FD = find(aux_FD,1,'first');

%% Cálculo de P_ISO

[m, ind_m]=min(dPLV);
aux = cumsum(dPLV(ind_m:length(dPLV))>(m*0.9));
[~, ind_2] = max(aux == 1);
ind_2 = ind_m + ind_2;

P_fit = [PLV(j,ind_FD:ind_M), PLV(j,ind_m:ind_2)];
T_fit = [t(j,ind_FD:ind_M), t(j,ind_m:ind_2)];

p = polyfit(T_fit, P_fit, 5);
Plv_est = polyval(p, t(j,ind_FD:ind_2));


[P_ISO, ind_ISO] = max(Plv_est);
V_ISO = VLV(j,ind_FD);


figure()
subplot(2,1,1)
plot(t(j,ind_FD:ind_2),Plv_est)
hold on
plot(t(j,:),PLV(j,:))
plot(T_fit, P_fit,'go')
plot(t(j,ind_ISO + ind_FD),P_ISO,'go')
hold off
subplot(2,1,2)
plot(t(j,:),dPLV)
hold on
plot(t(j,ind_FD),dPLV(ind_FD),'o')
plot(t(j,ind_M),M,'o')
plot(t(j,ind_m),m,'o')
plot(t(j,ind_2),dPLV(ind_2),'o')
hold off

%% Cálculo de P_ES y E_ES
V0 = 0;
aux_V0 = 1;

while abs(V0-aux_V0)>(1e-3)
    
    ELV = PLV(j,:)./(VLV(j,:)-V0);
    [~,ind_ES] = max(ELV);

    P_ES = PLV(j,ind_ES);
    V_ES = VLV(j,ind_ES);

    % Cálculo de elastancia máxima
    aux_V0 = V0;
    
    E_ES = (P_ISO-P_ES)/(V_ISO-V_ES);
    k = P_ES - (E_ES*V_ES);
    V0 = -k/E_ES;
end

figure()
plot(VLV(j,:),PLV(j,:))
hold on
plot(V_ES,P_ES,'o')
plot(V_ISO,P_ISO,'o')
plot([V0, V_ISO],E_ES*[V0, V_ISO] + k)
plot(VLV(j,ind_FD),PLV(j,ind_FD),'o')
hold off

%% Encontramos el punto de comienzo de eyección 

% % como el 10% previo al máximo de la derivada de presión
% aux = cumsum(dPLV(1:ind_M)>(M*0.9));
% [~, ind_CE] = max(aux == 1);

% pico negativo de d2P/dt2 
d2PLV = gradient(dPLV);
[~, ind_CE] = min(d2PLV(1:ind_ES));

P_CE = PLV(j,ind_CE);
V_CE = VLV(j,ind_CE);

figure()
subplot(3,1,1)
plot(t(j,:),PLV(j,:))
hold on
plot(t(j,ind_CE),PLV(j,ind_CE),'o')
subplot(3,1,2)
plot(t(j,:),dPLV)
hold on
plot(t(j,ind_CE),dPLV(ind_CE),'o')
subplot(3,1,3)
plot(t(j,:),d2PLV)
hold on
plot(t(j,ind_CE),d2PLV(ind_CE),'o')

%% Cálculo de Eap y Evi
ind_S = (ind_CE + 1):ind_ES;
Eap = zeros(1,length(ind_S));
Evi = zeros(1,length(ind_S));

for i = ind_S
    Eap(i-ind_CE) = (P_CE - PLV(j,i))/(V_CE - VLV(j,i));
    Evi(i-ind_CE) = (PLV(j,i))/(VLV(j,i)-V0);
end

ind_valido = Eap<0;

figure()
plot(abs(Eap(ind_valido)))
hold on
plot(Evi(ind_valido))
ylim([0 max(Evi)+3])
hold off

figure()
plot(PLV(j,ind_S.*ind_valido),abs(Eap(ind_valido)))
hold on
plot(PLV(j,ind_S.*ind_valido),Evi(ind_valido))
ylim([0 max(Evi)+3])
hold off

%% Valor de P?
inters = abs(Eap) <= Evi;
aux_IG = find(inters.*ind_S,1,'first');
ind_IG = ind_S(aux_IG);

figure()
plot(t(j,:),PLV(j,:))
hold on
plot(t(j,ind_ES),PLV(j,ind_ES),'bo')
plot(t(j,ind_CE),PLV(j,ind_CE),'ro')
plot(t(j,ind_IG),PLV(j,ind_IG),'go')
hold off
legend('P_{LV}', ['P_{FS} =' num2str(PLV(j,ind_ES))],['P_{CE} =' num2str(PLV(j,ind_CE))],['P_? =' num2str(PLV(j,ind_IG))]);
%
figure()
plot(VLV(j,:),PLV(j,:))
hold on
plot(V_ES,P_ES,'bo')
plot(V_CE,P_CE,'ro')
plot(VLV(j,ind_IG),PLV(j,ind_IG),'go')
plot([V0, VLV(j,1)],E_ES*[V0, VLV(j,1)] + k)

plot(V_ES,P_ES,'o')
plot(V_ISO,P_ISO,'o')
plot([V0, V_ISO],E_ES*[V0, V_ISO] + k)
plot(VLV(j,ind_FD),PLV(j,ind_FD),'o')
hold off

legend('Bucle PV_{LV}', 'Fin de sístole','Comienzo de eyección','Punto de intersección','Recta de E_{ES}');
