clc
close all

%% Datos PV

i=2;

filename = strcat(['Datos_oveja/Datos_PV_Armentano_Corr_',num2str(i),'.txt']);

opts = detectImportOptions(filename);
T = readtable(filename, opts);   %Datos Ventriculares

dt = 1/250;

%% Hallar filtro de fc
fs=1/dt;
min_fc = 20/60; %Hz
bpFilt = designfilt('bandpassfir', 'StopbandFrequency1', 0.25, 'PassbandFrequency1', 0.8, 'PassbandFrequency2', 2.8, 'StopbandFrequency2', 3.25, 'StopbandAttenuation1', 60, 'PassbandRipple', 1, 'StopbandAttenuation2', 60, 'SampleRate', fs);
 

%% Filtrado
L=10; 
b = (1/L)*ones(1,L);
a = 1;

T.PVIc = filter(b,a,T.Var1);
T.PAoc = filter(b,a,T.Var3);
% T.PVIc = T.PVIc-min(T.PVIc);

T.VVIc = filter(b,a,T.Var2);

%% Detección de corte

indI = Valentinuzzi(T.PVIc(40:length(T.PVIc)), dt,bpFilt);
indI = indI + 40;

%% Separación de ciclos

%Separación de ciclos

PLV = cut_beat(T.PVIc,indI,1);
PAo = cut_beat(T.PAoc,indI,1);
VLV = cut_beat(T.VVIc,indI,1);

%% parametros
ind_FDv = zeros(1,length(VLV(:,1)));
ind_ESv = zeros(1,length(VLV(:,1)));
ind_CEv = zeros(1,length(VLV(:,1)));
ind_IGv = zeros(1,length(VLV(:,1)));

P_ISOv = zeros(1,length(VLV(:,1)));
V_ISOv = zeros(1,length(VLV(:,1)));

E_ESv = zeros(1,length(VLV(:,1)));
V0_SBv = zeros(1,length(VLV(:,1)));


%% Modificación de volumen
V_lv = zeros(size(VLV));
V_modif = zeros(size(VLV));
ada = [1:length(VLV(:,1))];


for j=(ada*-1)+(ada(length(ada))+ada(1))
    
    dVLV=gradient(VLV(j,:));
   
    [m,ind_m] = findpeaks(-dVLV);
    [Max,ind_Max] = max(VLV(j,:));
    
    d2VLV=gradient(dVLV);
    
    val = find(ind_m>ind_Max,1);

    if j==1
        val = val + 1;
    end
    
    [~,c1] = max(d2VLV(ind_m(val):ind_m(val+1)));
    [~,c2] = min(d2VLV(ind_m(val):ind_m(val+1)));
    c1 = c1+ind_m(val);
    c2 = c2+ind_m(val);  

    aux_t = ones(1,c2-c1-1);
    aux_t=1:length(aux_t);
    aux_M = 0.3*cos(2*pi/(length(aux_t))*aux_t);
    aux_M = aux_M - aux_M(1) + Max;

    V_modif(j,:) = [VLV(j,1:ind_Max), aux_M, VLV(j,ind_Max:c1), ((VLV(j,c1)-VLV(j,c1-1))+VLV(j,c1)-VLV(j,c2+1)) + VLV(j,c2+1:length(VLV(j,:)))];

    L=5; 
    b = (1/L)*ones(1,L);
    a = 1;
    
    if j==length(VLV(:,1))
        V_aux = [((VLV(j,c1)-VLV(j,c1-1))+VLV(j,c1)-VLV(j,c2+1)) + VLV(j,c2+1:length(VLV(j,:))), T.VVIc(indI(j)+2:indI(j)+c2-c1+1)'];
    else
        V_aux = [((VLV(j,c1)-VLV(j,c1-1))+VLV(j,c1)-VLV(j,c2+1)) + VLV(j,c2+1:length(VLV(j,:))), V_modif(j+1,2:c2-c1+1)];
    end
    
    V_prueba = filtfilt(b,a,(V_aux - V_aux(1))) + V_aux(1);
    
    V_lv(j,:) = [V_modif(j,c2-c1+1:c1+length(aux_M)+1), V_prueba];
%     if j==length(VLV(:,1))
%         V_lv(j,:) = [V_modif(j,c2-c1+1:length(V_modif(j,:))), T.VVIc(indI(j)+2:indI(j)+c2-c1+1)'];
%     else
%         V_lv(j,:) = [V_modif(j,c2-c1+1:length(V_modif(j,:))), V_modif(j+1,2:c2-c1+1)];
%     end
    
%     figure(j+40)
%     subplot(4,1,1)
%     plot(PLV(j,:))
%     subplot(4,1,2)
%     plot(VLV(j,:))
%     hold on
%     plot(V_lv(j,:))
%     hold off
%     subplot(4,1,3)
%     plot(dVLV)
%     hold on
%     plot(ind_m,-m,'go')
%     plot(ind_Max,dVLV(ind_Max),'ro')
%     hold off
%     subplot(4,1,4)
%     plot(d2VLV)
%     hold on
%     plot(c1,d2VLV(c1),'bo')
%     plot(c2,d2VLV(c2),'mo')
%     hold off

%     plot(VLV(j,:),PLV(j,:))
%     hold on
%     plot(V_lv(j,:),PLV(j,:))
%    
end 

%% Encontramos el punto de fin de diástole
j=6;


t = 0:dt:(length(PLV(j,:))-1)*dt;
ind_FD = 9;
% f = figure()
% plot(V_lv(j,:),PLV(j,:))
% hold on
%     [V_FD,P_FD] = getpts(f);
%     ind1 = V_lv(j,:)== V_FD;
%     ind2 = PLV(j,:)== P_FD;
%     ind_FD = ind1 && ind2;
dPLV = gradient(PLV(j,:));
medio = mean(dPLV);
[M, ind_M]=max(dPLV);
% umbral = medio + 0.1*(M-medio);
% aux_FD = cumsum(dPLV(1:ind_M) > umbral);
% 
% ind_FD = find(aux_FD,1,'first');


% ind_FDv(j)=ind_FD;
%% Cálculo de P_ISO

[m, ind_m]=min(dPLV);
aux = cumsum(dPLV(ind_m:length(dPLV))>(m*0.9));
[~, ind_2] = max(aux == 1);
ind_2 = ind_m + ind_2;

P_fit = [PLV(j,ind_FD:ind_M), PLV(j,ind_m:ind_2)];
T_fit = [t(ind_FD:ind_M), t(ind_m:ind_2)];

p = polyfit(T_fit, P_fit, 5);
Plv_est = polyval(p, t(ind_FD:ind_2));


[P_ISO, ind_ISO] = max(Plv_est);
% V_ISO = VLV(j,ind_FD);
% V_ISO = V_CE

V_ISO = V_lv(j,ind_FD);

P_ISOv(j)=P_ISO;
V_ISOv(j)=V_ISO;

figure()
subplot(2,1,1)
plot(t(ind_FD:ind_2),Plv_est)
hold on
plot(t,PLV(j,:))
plot(T_fit, P_fit,'go')
plot(t(ind_ISO + ind_FD),P_ISO,'go')
hold off
subplot(2,1,2)
plot(t,dPLV)
hold on
plot(t(ind_FD),dPLV(ind_FD),'o')
plot(t(ind_M),M,'o')
plot(t(ind_m),m,'o')
plot(t(ind_2),dPLV(ind_2),'o')
hold off


%% Cálculo de P_ES y E_ES
V0 = 0;
aux_V0 = 1;

ELV = PLV(j,:)./(V_lv(j,:)-V0);
[~,ind_ES] = max(ELV);

P_ES = PLV(j,ind_ES);
V_ES = V_lv(j,ind_ES);

% Cálculo de elastancia máxima
aux_V0 = V0;

E_ES = (P_ISO-P_ES)/(V_ISO-V_ES);
k = P_ES - (E_ES*V_ES);
V0 = -k/E_ES;

% while abs(V0-aux_V0)>(1e-3)
%     
%     ELV = PLV(j,:)./(VLV(j,:)-V0);
%     [~,ind_ES] = max(ELV);
% 
%     P_ES = PLV(j,ind_ES);
%     V_ES = VLV(j,ind_ES);
% 
%     % Cálculo de elastancia máxima
%     aux_V0 = V0;
%     
%     E_ES = (P_ISO-P_ES)/(V_ISO-V_ES);
%     k = P_ES - (E_ES*V_ES);
%     V0 = -k/E_ES;
% end

figure()
plot(V_lv(j,:),PLV(j,:))
hold on
plot(VLV(j,:),PLV(j,:))
plot(V_ES,P_ES,'o')
plot(V_ISO,P_ISO,'o')
plot([V0, V_ISO],E_ES*[V0, V_ISO] + k)
plot(V_lv(j,ind_FD),PLV(j,ind_FD),'o')
hold off

E_ESv(j) = E_ES;
V0_SBv(j) = V0;
%% Encontramos el punto de comienzo de eyección 

% % como el 10% previo al máximo de la derivada de presión
% aux = cumsum(dPLV(1:ind_M)>(M*0.9));
% [~, ind_CE] = max(aux == 1);

% pico negativo de d2P/dt2 
d2PLV = gradient(dPLV);
[~, ind_CE] = min(d2PLV(1:ind_ES));
ind_CEv(j) = ind_CE;

P_CE = PLV(j,ind_CE);
V_CE = V_lv(j,ind_CE);

figure()
subplot(3,1,1)
plot(t,PLV(j,:))
hold on
plot(t(ind_CE),PLV(j,ind_CE),'o')
subplot(3,1,2)
plot(t,dPLV)
hold on
plot(t(ind_CE),dPLV(ind_CE),'o')
subplot(3,1,3)
plot(t,d2PLV)
hold on
plot(t(ind_CE),d2PLV(ind_CE),'o')

%% Cálculo de Eap y Evi
ind_S = (ind_CE + 1):ind_ES;
Eap = zeros(1,length(ind_S));
Evi = zeros(1,length(ind_S));

for i = ind_S
    Eap(i-ind_CE) = (P_CE - PLV(j,i))/(V_CE - V_lv(j,i));
    Evi(i-ind_CE) = (PLV(j,i))/(VLV(j,i)-V0);
end

ind_valido = Eap<0;

figure()
plot(abs(Eap(ind_valido)))
hold on
plot(Evi(ind_valido))
ylim([0 max(Evi)+3])
hold off

figure()
plot(PLV(j,ind_S(ind_valido)),abs(Eap(ind_valido)))
hold on
plot(PLV(j,ind_S(ind_valido)),Evi(ind_valido))
ylim([0 max(Evi)+3])
hold off

%% Valor de P?
inters = abs(Eap) <= Evi;
aux_IG = find(inters.*ind_S,1,'first');
ind_IG = ind_S(aux_IG);
ind_IGv(j)=ind_IG;

Psis = max(PAo(j,:));
Pdia = min(PAo(j,:));
Pmed = (2*Pdia + Psis)/3;

auxP = ones(1,length(t))*Pmed;
figure()
plot(t,PLV(j,:))
hold on
plot(t(ind_ES),PLV(j,ind_ES),'bo')
plot(t(ind_CE),PLV(j,ind_CE),'ro')
plot(t(ind_IG),PLV(j,ind_IG),'go')
plot(t,PAo(j,:))
plot(t,auxP)
hold off
legend('P_{LV}', ['P_{FS} =' num2str(PLV(j,ind_ES))],['P_{CE} =' num2str(PLV(j,ind_CE))],['P_? =' num2str(PLV(j,ind_IG))]);
%
figure()
plot(V_lv(j,:),PLV(j,:))
hold on
plot(V_ES,P_ES,'bo')
plot(V_CE,P_CE,'ro')
plot(V_lv(j,ind_IG),PLV(j,ind_IG),'go')
plot([V0, V_lv(j,1)],E_ES*[V0, V_lv(j,1)] + k)
hold off
legend('Bucle PV_{LV}', 'Fin de sístole','Comienzo de eyección','Punto de intersección','Recta de E_{ES}');

%
% a = 1:length(VLV(:,1));
% for j=(a*-1)+(a(length(a))+a(1))
%     figure(j)
%     plot(V_lv(j,:),PLV(j,:))
%     hold on
%     plot(VLV(j,:),PLV(j,:))
%     hold off

%% Cálculo de elastancia

tol = 0.5;
% [Emax, V0, Vfs, Pfs, e] = iteracion(PLV, VLV, tol);
ind = [1:21, 25:length(PLV(:,1))];
[Emax, V0, Vfs, Pfs, e] = iteracion(PLV(ind,:), VLV(ind,:), tol);
% [Emax, V0, Vfs, Pfs, e] = iteracion(PLV(1:length(PLV(:,1))-12,:), VLV(1:length(PLV(:,1))-12,:), tol);

figure()
for i=ind
    hold on
    plot(VLV(i,:),PLV(i,:))
end
plot(Vfs,Pfs,'k*')
V = [min(T.VVIc), max(Vfs)+10];
plot(V,Emax*(V-V0), 'LineWidth',1.5,'Color','b')

plot(VLV(j,:),PLV(j,:),'LineWidth',3)

% plot(V_ES,P_ES,'o')
% plot(V_ISO,P_ISO,'o')
% plot([V0, V_ISO],E_ES*[V0, V_ISO] + k)
% plot(VLV(j,ind_FD),PLV(j,ind_FD),'o')
hold off

%% Cálculo de elastancia (Volumen modificado)

tol = 0.5;
% [Emax2, V02, Vfs2, Pfs2, e2] = iteracion(PLV, V_lv, tol);
ind = [1:21, 25:length(PLV(:,1))];
[Emax2, V02, Vfs2, Pfs2, e2] = iteracion(PLV(ind,:), V_lv(ind,:), tol);
% [Emax2, V02, Vfs2, Pfs2, e2] = iteracion(PLV(1:length(PLV(:,1))-12,:), V_lv(1:length(PLV(:,1))-12,:), tol);

figure()
for i=ind
    hold on
    plot(V_lv(i,:),PLV(i,:))
end
plot(Vfs2,Pfs2,'k*')
V = [min(T.VVIc), max(Vfs2)+10];
plot(V,Emax2*(V-V02), 'LineWidth',1.5,'Color','b')

plot(V_lv(j,:),PLV(j,:),'LineWidth',3)

% plot(V_ES,P_ES,'o')
% plot(V_ISO,P_ISO,'o')
% plot([V0, V_ISO],E_ES*[V0, V_ISO] + k)
% plot(V_lv(j,ind_FD),PLV(j,ind_FD),'o')
hold off

% Comparación
% figure()
% hold on
% plot(Vfs,Pfs,'bo')
% V = [min(T.VVIc), max(Vfs)+10];
% plot(V,Emax*(V-V0), 'LineWidth',1.5,'Color','b')
% 
% plot(VLV(j,:),PLV(j,:),'LineWidth',3,'Color','g')
% 
% plot(Vfs2,Pfs2,'ro')
% V = [min(T.VVIc), max(Vfs2)+10];
% plot(V,Emax2*(V-V02), 'LineWidth',1.5,'Color','r')
% 
% plot(V_lv(j,:),PLV(j,:),'LineWidth',3, 'Color','m')
