function [ind_FD, ind_ES, E_ES, V0, P_ISO]=latido_unico(t, P, V)

    % Encontramos el punto de fin de diástole .............................
    dP = gradient(P);
    medio = mean(dP);
    [M, ind_M]=max(dP);
    umbral = medio + 0.1*(M-medio);
    aux_FD = cumsum(dP(1:ind_M) > umbral);

    ind_FD = find(aux_FD,1,'first');
    
    % Cálculo de P_ISO ....................................................
    [m, ind_m]=min(dP);
    aux = cumsum(dP(ind_m:length(dP))>(m*0.1));
    [~, ind_2] = max(aux == 1);
    ind_2 = ind_m + ind_2;

    P_fit = [P(ind_FD:ind_M), P(ind_m:ind_2)];
    T_fit = [t(ind_FD:ind_M), t(ind_m:ind_2)];

    [p, S, mu] = polyfit(T_fit, P_fit, 5);
    [Plv_est,~] = polyval(p,t(ind_FD:ind_2),S,mu);


    [P_ISO, ind_ISO] = max(Plv_est);
    V_ISO = V(ind_FD);

    % Cálculo de P_ES y E_ES ..............................................
    V0 = 0;
    aux_V0 = 1;

    while abs(V0-aux_V0)>(1e-3)

        ELV = P./(V-V0);
        [~,ind_ES] = max(ELV);

        P_ES = P(ind_ES);
        V_ES = V(ind_ES);

        % Cálculo de elastancia máxima
        aux_V0 = V0;

        E_ES = (P_ISO-P_ES)/(V_ISO-V_ES);
        k = P_ES - (E_ES*V_ES);
        V0 = -k/E_ES;
    end

    figure(1)
    subplot(2,2,1)
    
     hold on
title('Análisis de latido único sobre la presión')
    plot(t(ind_FD:ind_2),Plv_est)
    plot(t,P)
    plot(T_fit, P_fit,'go')
    plot(t(ind_ISO + ind_FD),P_ISO,'go')
    hold off
    xlabel('Tiempo (s)')
    ylabel('Presión (mmHg)')
    legend('Ajuste polinomial','Presión ventricular','Puntos del ajuste','Presión isovolúmica')
    xlim([min(t) max(t)])
    
    subplot(2,2,3)
    hold on
    
title('Entrada al modelo de corazón')
    yyaxis left
    plot(t,ELV)
    plot(t(ind_ES),ELV(ind_ES),'o')
    ylabel('Elastancia (mmHg/ml)')
    xlim([min(t) max(t)])
    
    yyaxis right
    plot(t,1./ELV)
    ylabel('Compliancia (ml/mmHg)')
    hold off
    xlabel('Tiempo (s)')
     legend('Elastancia','Elastancia Máxima','Compliancia')
    xlim([min(t) max(t)])
    
    subplot(1,2,2)
    hold on
    plot(V,P)
    plot(V_ES,P_ES,'o')
    plot(V_ISO,P_ISO,'o')
    plot([V0, V_ISO],E_ES*[V0, V_ISO] + k)
    plot(V0,0,'o')
title('Bucle PV del ventrículo izquierdo')
    hold off
% %     xlabel('Volumen (ml)')
% %     ylabel('Presión (mmHg)')
% %     legend('Bucle PV ventricular','Fin de sístole','Punto isovolumétrico','Recta de E_{ES}','V_0')
end