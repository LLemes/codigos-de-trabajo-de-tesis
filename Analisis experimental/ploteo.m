E2 = interp1(t,E_vi,Pao.time);
P2 = interp1(t,P,Pao.time);
[~,ind_ES2]=max(E2);
dP = gradient(P2(:))/dt;
% pico negativo de d2P/dt2 
d2P = gradient(dP)/dt;
[~, ind_CE] = min(d2P(1:ind_ES2-5));

P_CE = P2(ind_CE);
V_CE = V(ind_CE);

Pao_CE = Pao.signals.values(ind_CE);
Vao_CE = VAo(ind_CE);


figure(300)
subplot(221)

hold on
% Flip bucle aortico
plot(V_CE-(VAo-Vao_CE),Pao.signals.values)
xlabel('Volumen (ml)')
ylabel('Presión (mmHg)')
hold off
title('Bucle PV arterial')

subplot(222)

hold on
% Flip bucle aortico
plot(V_CE-(VAo-Vao_CE),Pao.signals.values)
xlabel('Volumen (ml)')
ylabel('Presión (mmHg)')
hold off
title('Bucle PV arterial')

subplot(223)

hold on
% Flip bucle aortico
plot(V_CE-(VAo-Vao_CE),Pao.signals.values)
xlabel('Volumen (ml)')
ylabel('Presión (mmHg)')
hold off
title('Bucle PV arterial')

subplot(224)

hold on
% Flip bucle aortico
plot(V_CE-(VAo-Vao_CE),Pao.signals.values)
xlabel('Volumen (ml)')
ylabel('Presión (mmHg)')
hold off
title('Bucle PV arterial')
