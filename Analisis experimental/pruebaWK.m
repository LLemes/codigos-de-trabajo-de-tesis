%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         Prueba de circuito WK
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Lt = 1;
%% Parámetros Modelo Circulacion
global Rv
global Pv
global Vd

Vd = 46.6281; %ml

Rv = 0.5444;
Pv = 39.8050;

% Elastancia

global T
global Tp
% global Emax
% global Emin
% 
% Emax = 2.31; %mmHg/ml
% Emin = 0.06; %mmHg/ml

T=1.0;


%%
% =========================================================================
%                    Flujo a partir de Datos de oveja
% =========================================================================
i=2;

filename = strcat(['Datos_oveja/Datos_PV_Armentano_Corr_',num2str(i),'.txt']);

opts = detectImportOptions(filename);
To = readtable(filename, opts);   %Datos Ventriculares

dt = 1/250;

% Filtrado
L=10; 
b = (1/L)*ones(1,L);
a = 1;

To.PVIc = filter(b,a,To.Var1);
To.PAoc = filter(b,a,To.Var3);
% To.PVIc = T.PVIc-min(To.PVIc);
To.VVIc = filter(b,a,To.Var2);

% Elastancia
PAoN = To.PAoc(128:270);
PN = To.PVIc(128:270);
VN = To.VVIc(128:270); 
EN = PN./(VN-Vd);     % Consideramos un latido como "molde"
% EN = EN*Emax/max(EN);   % Se "ajusta" a los parámetros de humano
% EN = EN-min(EN)+Emin;
[~,ind_p]=max(EN);

E = EN; 
P = PN;
V = VN;
PAo = PAoN; 
for i=1:Lt-1
    E = cat(1,E,EN);    % Se repiten los ciclos
    P = cat(1,P,PN);    % Se repiten los ciclos
    V = cat(1,V,VN);    % Se repiten los ciclos
    PAo = cat(1,PAo,PAoN);    % Se repiten los ciclos
end

tN = linspace(0,T,length(PN));
Et = linspace(0,Lt*T,length(E));
Tp = Et(ind_p);

%% Flujo

Q = -gradient(V)/dt;

Q2 = (P - Pv)/Rv;

dP = gradient(P)/dt;
dE = gradient(E)/dt;
Q3 = -(dP - dE.*P./E)./E;
% Q2(Q2<0)=0;

flujo=2;

if flujo==1
    Q_modelo = zeros(length(Q),2);
    Q_modelo(:,1)=Et;
    Q_modelo(:,2)=Q;
else if flujo==2
    Q_modelo = zeros(length(Q2),2);
    Q_modelo(:,1)=Et;
    Q_modelo(:,2)=Q2;
    else
        Q_modelo = zeros(length(Q3),2);
        Q_modelo(:,1)=Et;
        Q_modelo(:,2)=Q3;
    end
end

%%
LVEDV = 95; %ml

[t,VLV] = ode45(@(t,y) Vdot(t,y,Et,E), [0 Lt], LVEDV);

%...........................................
E = interp1(Et,E,t);
PLV = E.*(VLV-Vd);

figure()
plot(t,VLV)
hold on
plot(Et,V)
hold off
legend('Volumen LV estimado','Volumen LV real')

figure()
plot(t,PLV)
hold on
plot(Et,P)
hold off
legend('Presión LV estimada','Presión LV real')

figure()
plot(VLV,PLV)
hold on
plot(V,P)
legend('Bucle PV estimado','Bucle PV real')