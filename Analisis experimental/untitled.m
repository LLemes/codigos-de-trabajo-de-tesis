
figure()

subplot(221)
% plot(t,P)
hold on
plot(Pao.time, Pao.signals.values)
plot(Pao_true(:,1),Pao_true(:,2))
axis tight
hold off
title('Presión')
ylabel('Presión (mmHg)')
xlabel('Tiempo (s)')
% legend('P_{LV}', 'P_{Ao} estimada por WK', 'P_{Ao} reconstruida');
legend('P_{Ao} estimada por WK', 'P_{Ao} reconstruida');

subplot(223)
E2 = interp1(t,E_vi,Pao.time);
P2 = interp1(t,P,Pao.time);
[~,ind_ES2]=max(E2);
dP = gradient(P2(:))/dt;
% pico negativo de d2P/dt2 
d2P = gradient(dP)/dt;
[~, ind_CE] = min(d2P(1:ind_ES2-5));

P_CE = P2(ind_CEj);
V_CE = V(ind_CEj);

Pao_CE = Pao.signals.values(ind_CEj);
Vao_CE = VAo(ind_CEj);
Vao2_CE = VAo2(ind_CEj);

plot(Pao.time,V_CE-(VAo-Vao_CE))
hold on
plot(t, V)
hold off
axis tight
title('Volumen')
ylabel('Volumen (ml)')
xlabel('Tiempo (s)')
legend('Volumen Arterial', 'Volumen ventricular izquierdo');

subplot(122)
hold on
plot(V,P)
plot(Vlv(j,:),Plv(j,:))
% Flip bucle aortico
plot(V_CE-(VAo-Vao_CE),Pao.signals.values)
plot(V_CE-(VAo2-Vao2_CE),Pao.signals.values)
ind_FD2 = length(V)+(ind_FD-30);
ind_ES2 = (ind_ES-30);
plot([V0 V(ind_FD)], [0 (E_ES*(V(ind_FD)-V0))])
plot([V(ind_FD2) V(ind_ES2)], [P(ind_FD2) P(ind_ES2)])

% plot(V_FS-(VAo-Vao_FS),Pao2)
xlabel('Volumen (ml)')
ylabel('Presión (mmHg)')
title('Bucle PV')
legend('Ventricular','Arterial estimado (MQM)','Arterial estimado (MQ)')
